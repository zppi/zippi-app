import "./src/polyfills";
import "date-time-format-timezone";
import { AppLoading } from "expo";
import { Asset } from "expo-asset";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import * as Font from "expo-font";
import React, { ReactNode } from "react";
import { KeyboardAvoidingView, StyleSheet, YellowBox } from "react-native";
import "./src/config";
import AppContainer from "./src/navigation/AppContainer";
import { fonts } from "./src/styles";
import ConfiguredApollo from "./src/config/ConfiguredApollo";

YellowBox.ignoreWarnings(["Overwriting fontFamily"]);
StyleSheet.setStyleAttributePreprocessor("fontFamily", Font.processFontFamily);

interface State {
  appLoaded: boolean;
  isAuthenticated: boolean | null;
}

export default class App extends React.Component<null, State> {
  state: State = { appLoaded: false, isAuthenticated: null };

  async componentDidMount(): Promise<void> {
    await Promise.all([
      Font.loadAsync({
        [fonts.default]: require("./assets/fonts/OpenSans-Regular.ttf"),
        [fonts.bold]: require("./assets/fonts/OpenSans-Bold.ttf"),
        [fonts.italic]: require("./assets/fonts/OpenSans-Italic.ttf"),
        ...MaterialCommunityIcons.font,
        ...Ionicons.font
      }),
      Asset.loadAsync(require("./assets/splash.png"))
    ]);

    this.setState({ appLoaded: true });
  }
  render(): ReactNode {
    if (!this.state.appLoaded) {
      return <AppLoading />;
    }
    return (
      <ConfiguredApollo>
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <AppContainer />
        </KeyboardAvoidingView>
      </ConfiguredApollo>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 }
});
