// https://www.apollographql.com/docs/references/apollo-config/

module.exports = {
  client: {
    service: {
      name: "Zippi Local",
      url: "http://localhost:2800/graphql"
    }
  }
};
