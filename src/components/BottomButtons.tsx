import React, { ReactNode } from "react";
import { TextStyle, View, TouchableOpacity, StyleSheet, Keyboard } from "react-native";
import { hitSlop, colors } from "../styles";
import Txt from "./Txt";

interface Props {
  buttons: any[];
  showOnKeyboard?: boolean;
}

interface State {
  display: boolean;
}

export default class BottomButtons extends React.Component<Props, State> {
  keyboardDidShowListener: any;
  keyboardDidHideListener: any;

  state = { display: true };
  display = true;

  componentDidMount(): void {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () =>
      this._keyboardDidShow()
    );
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () =>
      this._keyboardDidHide()
    );
  }

  componentWillUnmount(): void {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(): void {
    if (!this.props.showOnKeyboard) {
      this.setState({ display: false });
    }
  }

  _keyboardDidHide(): void {
    this.setState({ display: true });
  }

  render(): ReactNode {
    let buttons: any[] = [];
    const width = 100 / this.props.buttons.length;
    const widthString = width.toString() + "%";
    const buttonStyle = {
      width: widthString,
      borderWidth: 0.5,
      borderColor: "#d6d7da"
    };
    this.props.buttons.forEach((button: any, i: number) => {
      buttons.push(
        <TouchableOpacity
          key={i}
          onPress={button.action}
          hitSlop={hitSlop.large}
          style={buttonStyle}
          disabled={button.disabled}
        >
          <Txt style={button.disabled ? styles.bottomTextDisabled : styles.bottomTextEnabled}>
            {button.text}
          </Txt>
        </TouchableOpacity>
      );
    });
    return (
      <View style={this.state.display ? styles.buttonsContainer : styles.noShow}>{buttons}</View>
    );
  }
}

const bottomText: TextStyle = {
  textAlign: "center",
  fontSize: 20,
  padding: 10
};

const styles = StyleSheet.create({
  buttonsContainer: {
    height: 45,
    flexDirection: "row"
  },
  bottomTextEnabled: {
    ...bottomText
  },
  bottomTextDisabled: {
    color: colors.lightGray,
    ...bottomText
  },
  noShow: {
    display: "none"
  }
});
