import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import React, { ReactNode } from "react";
import { StyleSheet, View, TouchableOpacity, LayoutAnimation } from "react-native";

import Txt from "./Txt";
import ItemThumbnail from "./ItemThumbnail";
import { Item } from "../models/models";
import { format, addSeconds } from "date-fns";
import { formattedReservation } from "../services/time";
import * as Localization from "expo-localization";
import { hitSlop } from "../styles";
import { withDropdown } from "../navigation/DropdownContainer";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { DeleteReservation, DeleteReservationVariables } from "./__generated__/DeleteReservation";
import { ITEM_DETAILS_QUERY } from "../screens/items/ItemDetails";

interface Props {
  item: Item;
}

export default class DisplayItem extends React.Component<Props> {
  formatMaxReservationRule(maxLength: number): string {
    let ruleEnd = "";
    if (maxLength > 60) {
      ruleEnd += maxLength >= 120 ? ` ${Math.round(maxLength / 60)} hours` : " 1 hour";
    }
    if (maxLength > 60 && maxLength % 60 > 0) {
      ruleEnd += " and";
    }
    if (maxLength % 60 > 0) {
      ruleEnd += maxLength % 60 > 1 ? ` ${maxLength % 60} minutes` : " 1 minute";
    }
    return ruleEnd;
  }

  reservationDeleted = () => {
    withDropdown(d => d.alertWithType("success", "Success", "Reservation cancelled"));
    LayoutAnimation.easeInEaseOut();
  };

  render(): ReactNode {
    const { item } = this.props;

    let date: Date = new Date();
    date.setHours(0, 0, 0, 0);
    const midnightDate: Date = date;
    const displayMaxLength =
      item.maxReservationLength !== null &&
      item.maxReservationLength !== undefined &&
      item.maxReservationLength !== 0;

    return (
      <>
        <ItemThumbnail item={item} style={styles.image} />
        <View style={styles.content}>
          <Txt title style={styles.title}>
            {item.name}
          </Txt>
          <Txt style={styles.description}>{item.description}</Txt>
          <Txt subtitle style={styles.subtitle}>
            Rules
          </Txt>
          {item.openTime !== null && (
            <View style={styles.rule}>
              <Ionicons name="md-checkmark" style={styles.ruleCheckmark} />
              <Txt>
                No reservation can begin earlier than{" "}
                {format(addSeconds(midnightDate, item.openTime), "h:mma")}
              </Txt>
            </View>
          )}
          {item.closeTime !== null && (
            <View style={styles.rule}>
              <Ionicons name="md-checkmark" style={styles.ruleCheckmark} />
              <Txt>
                No reservation can end later than{" "}
                {format(addSeconds(midnightDate, item.closeTime), "h:mma")}
              </Txt>
            </View>
          )}
          {displayMaxLength && item.maxReservationLength && (
            <View style={styles.rule}>
              <Ionicons name="md-checkmark" style={styles.ruleCheckmark} />
              <Txt>
                Max reservation length:
                {this.formatMaxReservationRule(item.maxReservationLength)}
              </Txt>
            </View>
          )}
          <View style={styles.rules}>
            {item.rules &&
              item.rules.map(r => (
                <View style={styles.rule} key={r}>
                  <Ionicons name="md-checkmark" style={styles.ruleCheckmark} />
                  <Txt>{r}</Txt>
                </View>
              ))}
          </View>
          <Txt subtitle style={styles.subtitle}>
            Reservations
          </Txt>
          {item.reservations &&
            (item.reservations.length === 0 ? (
              <Txt style={{ textAlign: "center" }}>No upcoming reservations</Txt>
            ) : (
              item.reservations.map(r => (
                <View style={styles.reservation} key={r.id}>
                  <MaterialCommunityIcons name="calendar" style={styles.reservationIcon} />
                  <View style={styles.reservationText}>
                    <Txt bold>
                      {formattedReservation(
                        r.startTime,
                        r.endTime,
                        item.timeZone || Localization.timezone
                      )}
                    </Txt>
                    <Txt>{r.user.email}</Txt>
                  </View>
                  {r.canDelete && (
                    <Mutation<DeleteReservation, DeleteReservationVariables>
                      mutation={gql`
                        mutation DeleteReservation($id: String!) {
                          deleteReservation(id: $id) {
                            id
                            success
                          }
                        }
                      `}
                      refetchQueries={[{ query: ITEM_DETAILS_QUERY, variables: { id: item.id } }]}
                      variables={{ id: r.id }}
                      onCompleted={this.reservationDeleted}
                    >
                      {deleteReservation => (
                        <TouchableOpacity
                          hitSlop={hitSlop.large}
                          onPress={() => deleteReservation()}
                        >
                          <MaterialCommunityIcons
                            name="close"
                            style={[styles.reservationIcon, styles.reservationDeleteIcon]}
                          />
                        </TouchableOpacity>
                      )}
                    </Mutation>
                  )}
                </View>
              ))
            ))}
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    backgroundColor: "#ccc"
  },
  content: {
    paddingHorizontal: 30,
    paddingVertical: 20
  },
  title: {
    marginBottom: 20
  },
  description: {
    marginBottom: 20
  },
  subtitle: {
    marginBottom: 10
  },
  rules: {
    marginBottom: 15
  },
  rule: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5
  },
  ruleCheckmark: {
    color: "#aaa",
    fontSize: 20,
    marginRight: 5
  },
  reservation: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10
  },
  reservationIcon: {
    color: "#aaa",
    fontSize: 30
  },
  reservationDeleteIcon: {
    marginRight: -5
  },
  reservationText: {
    flex: 1,
    paddingHorizontal: 5
  },
  headerShadow: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 80
  }
});
