import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import React, { ReactNode } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
  Image,
  FlatList,
  TouchableOpacity
} from "react-native";
import { TextField } from "react-native-material-textfield";
import * as ImagePicker from "expo-image-picker";
import DateTimePicker from "react-native-modal-datetime-picker";
import { format, addSeconds } from "date-fns";

import Txt from "./Txt";
import { hitSlop, colors } from "../styles";

interface Props {
  item: any;
  isUploading: boolean;
  updateName: any;
  updateDescription: any;
  updateImage: any;
  updateOpenTime: any;
  updateCloseTime: any;
  updateRules: any;
  updateMaxReservationLength: any;
}

interface State {
  isOpenTimePickerVisible: boolean;
  isCloseTimePickerVisible: boolean;
  isMaxTimePickerVisible: boolean;
  rules: string[] | null;
  newRule: string;
  refreshRules: boolean;
}

export default class EditItem extends React.Component<Props, State> {
  state: State = {
    isOpenTimePickerVisible: false,
    isCloseTimePickerVisible: false,
    isMaxTimePickerVisible: false,
    rules: null,
    newRule: "",
    refreshRules: false
  };

  _pickImage = async (imageType: string) => {
    const options = {
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1] as [number, number]
    };
    let result =
      imageType === "camera"
        ? await ImagePicker.launchCameraAsync(options)
        : await ImagePicker.launchImageLibraryAsync(options);

    if (!result.cancelled) {
      this.props.updateImage(result.uri);
    }
  };

  secondsFromMidnight(date: Date | null): number | null {
    if (!date) {
      return null;
    }
    return date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
  }

  handleDatePicked = (date: Date | null, whichTime: string) => {
    if (whichTime === "open") {
      this.props.updateOpenTime(this.secondsFromMidnight(date));
      this.setState({ isOpenTimePickerVisible: false });
    } else if (whichTime === "close") {
      this.props.updateCloseTime(this.secondsFromMidnight(date));
      this.setState({ isCloseTimePickerVisible: false });
    } else if (whichTime === "max") {
      if (date) {
        this.props.updateMaxReservationLength(date.getHours() * 60 + date.getMinutes());
      } else {
        this.props.updateMaxReservationLength(null);
      }
      this.setState({ isMaxTimePickerVisible: false });
    }
  };

  updateRule = async (rule: string, index: number) => {
    if (!this.state.rules) return;
    let rules: string[] = this.state.rules;
    rules[index] = rule;
    this.setState({ rules });
  };

  addRule = async (rule: string) => {
    if (!this.state.rules) return;
    let rules: string[] = this.state.rules;
    rules.push(rule);
    this.setState({ newRule: "" });
    this.setState({ rules, refreshRules: !this.state.refreshRules });
    this.props.updateRules(rules);
  };

  deleteRule = async (index: number) => {
    if (!this.state.rules) return;
    let rules: string[] = this.state.rules;
    rules.splice(index, 1);
    this.setState({ rules });
    this.props.updateRules(rules);
    this.setState({ rules, refreshRules: !this.state.refreshRules });
  };

  makeTimeOfDay(seconds: number | null): Date | null {
    if (seconds === null) {
      return null;
    }
    let d = new Date();
    d.setHours(0, 0, 0, 0);
    return addSeconds(d, seconds);
  }

  maxToDate(): Date | null {
    let d = new Date();
    if (this.props.item.maxReservationLength) {
      // prettier-ignore
      d.setHours(this.props.item.maxReservationLength / 60, this.props.item.maxReservationLength % 60, 0, 0);
      return d;
    }
    return null;
  }

  componentDidUpdate(): void {
    if (!this.state.rules && this.props.item) {
      this.setState({ rules: this.props.item.rules });
    }
  }

  render(): ReactNode {
    const { item, isUploading, updateName, updateDescription, updateRules } = this.props;
    if (!item) {
      return null;
    }

    const openTimeDate = this.makeTimeOfDay(item.openTime);
    const closeTimeDate = this.makeTimeOfDay(item.closeTime);
    const maxDate = item.maxReservationLength
      ? this.makeTimeOfDay(item.maxReservationLength * 60)
      : null;

    return (
      <>
        <View style={[StyleSheet.absoluteFill, { backgroundColor: "white" }]}>
          <View style={{ height: "50%", backgroundColor: "black" }} />
        </View>
        <StatusBar barStyle="light-content" />
        <ScrollView contentContainerStyle={{ backgroundColor: "white" }}>
          <View style={styles.container}>
            <View style={styles.backgroundContainer}>
              {item.imgPath ? (
                <Image source={{ uri: item.imgPath }} resizeMode="cover" style={styles.image} />
              ) : (
                <View style={styles.image} />
              )}
            </View>
            <View style={styles.overlay}>
              {!isUploading && (
                <View>
                  <View style={{ flexDirection: "row" }}>
                    <MaterialCommunityIcons
                      onPress={() => this._pickImage("camera")}
                      name="camera"
                      style={{ fontSize: 40, color: "white", padding: 10 }}
                    />
                    <MaterialCommunityIcons
                      onPress={() => this._pickImage("imageLibrary")}
                      name="file-image"
                      style={{ fontSize: 40, color: "white", padding: 10 }}
                    />
                  </View>
                </View>
              )}
            </View>
          </View>

          <View style={styles.content}>
            <TextField label="Item Name" value={item.name} onChangeText={updateName} />
            <TextField
              label="Description"
              multiline={true}
              value={item.description}
              onChangeText={updateDescription}
            />
            <TimeRow
              label="Max Reservation Length"
              hideAMPM={true}
              date={maxDate}
              onDatePress={() => this.setState({ isMaxTimePickerVisible: true })}
              onDeletePress={() => this.handleDatePicked(null, "max")}
            />

            <TimeRow
              label="Open Time"
              date={openTimeDate}
              onDatePress={() => this.setState({ isOpenTimePickerVisible: true })}
              onDeletePress={() => this.handleDatePicked(null, "open")}
            />
            <TimeRow
              label="Close Time"
              date={closeTimeDate}
              onDatePress={() => this.setState({ isCloseTimePickerVisible: true })}
              onDeletePress={() => this.handleDatePicked(null, "close")}
            />
            <Txt title style={{ paddingTop: 30, flex: 10 }}>
              Rules
            </Txt>

            {this.state.rules && (
              <FlatList
                data={this.state.rules}
                renderItem={({ item, index }) => (
                  <View style={[styles.rule]}>
                    <View style={{ flex: 10 }}>
                      <TextField
                        label={""}
                        labelHeight={0}
                        placeholder="Rule"
                        value={item}
                        onChangeText={(text: string) => this.updateRule(text, index)}
                        onBlur={() => updateRules(this.state.rules)}
                      />
                    </View>
                    <View style={[styles.buttonContainer]}>
                      <TouchableOpacity
                        style={styles.deleteButton}
                        onPress={() => this.deleteRule(index)}
                        hitSlop={hitSlop.large}
                      >
                        <MaterialCommunityIcons name="minus" style={styles.deleteButtonIcon} />
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
                keyExtractor={(i: string) => i.toString()}
                extraData={this.state.refreshRules}
              />
            )}

            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 10 }}>
                <TextField
                  label=""
                  placeholder="New Rule"
                  labelHeight={5}
                  value={this.state.newRule}
                  onChangeText={(text: string) => this.setState({ newRule: text })}
                />
              </View>
              <View style={styles.buttonContainer}>
                {this.state.newRule !== "" && (
                  <TouchableOpacity
                    hitSlop={hitSlop.large}
                    style={styles.deleteButton}
                    onPress={() => this.addRule(this.state.newRule)}
                  >
                    <MaterialCommunityIcons name="plus" style={styles.addButtonIcon} />
                  </TouchableOpacity>
                )}
              </View>
            </View>

            <DateTimePicker
              mode="time"
              date={openTimeDate || undefined}
              is24Hour={false}
              timePickerModeAndroid="spinner"
              isVisible={this.state.isOpenTimePickerVisible}
              onConfirm={date => this.handleDatePicked(date, "open")}
              onCancel={() => this.setState({ isOpenTimePickerVisible: false })}
            />
            <DateTimePicker
              mode="time"
              date={closeTimeDate || undefined}
              is24Hour={false}
              timePickerModeAndroid="spinner"
              isVisible={this.state.isCloseTimePickerVisible}
              onConfirm={date => this.handleDatePicked(date, "close")}
              onCancel={() => this.setState({ isOpenTimePickerVisible: false })}
            />
            <DateTimePicker
              mode="time"
              date={maxDate || this.makeTimeOfDay(0) || undefined}
              timePickerModeAndroid="spinner"
              isVisible={this.state.isMaxTimePickerVisible}
              onConfirm={date => this.handleDatePicked(date, "max")}
              onCancel={() => this.setState({ isMaxTimePickerVisible: false })}
            />
          </View>
        </ScrollView>
        <LinearGradient
          colors={["rgba(0, 0, 0, 0.4)", "transparent"]}
          style={styles.headerShadow}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    backgroundColor: "#ccc"
  },
  content: {
    paddingHorizontal: 30,
    paddingBottom: 20
  },
  title: {
    marginBottom: 20,
    fontSize: 30
  },
  description: {
    marginBottom: 20
  },
  subtitle: {
    marginTop: 10,
    marginBottom: 10
  },
  rules: {
    marginBottom: 15
  },
  rule: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10
  },
  ruleCheckmark: {
    color: "#aaa",
    fontSize: 20,
    marginRight: 5
  },
  headerShadow: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 80
  },
  container: {
    flex: 1,
    alignItems: "center",
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    backgroundColor: "#ccc"
  },
  backgroundContainer: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  overlay: {
    opacity: 0.5,
    backgroundColor: "black",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    width: "100%",
    height: undefined,
    aspectRatio: 1
  },
  buttonContainer: {
    flex: 1,
    paddingRight: 0,
    justifyContent: "center"
  },
  deleteButton: {
    alignItems: "center",
    alignContent: "center"
  },
  deleteButtonIcon: { color: "red", fontSize: 30 },
  addButtonIcon: { color: colors.green, fontSize: 30 }
});

interface TimeRowProps {
  label: string;
  date: Date | null;
  onDatePress: any;
  onDeletePress: any;
  hideAMPM?: boolean;
}

const TimeRow = class TimeRow extends React.Component<TimeRowProps> {
  displayTime = (date: Date | null): string => {
    if (!date) {
      return "none";
    }
    if (this.props.hideAMPM) {
      let hours;
      if (date.getHours() === 0) {
        hours = "00";
      } else {
        hours = format(date, "HH");
      }
      return hours + format(date, ":mm");
    }
    return date ? format(date, "h:mm a") : "None";
  };

  render(): ReactNode {
    const { label, date, onDatePress, onDeletePress } = this.props;
    return (
      <View style={styles.rule}>
        <Txt subtitle style={{ paddingTop: 3, flex: 5 }}>
          {label}
        </Txt>
        <View
          style={{
            borderColor: colors.darkGray,
            borderWidth: 1,
            padding: 3,
            margin: 10,
            flex: 5,
            justifyContent: "center"
          }}
        >
          <TouchableOpacity onPress={onDatePress}>
            <Txt subtitle style={{ textAlign: "center" }}>
              {this.displayTime(date)}
            </Txt>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonContainer}>
          {date && (
            <TouchableOpacity style={styles.deleteButton} onPress={onDeletePress}>
              <MaterialCommunityIcons name="minus" style={styles.deleteButtonIcon} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
};
