import React, { ReactElement } from "react";
import { StyleProp, View, Image, ImageStyle } from "react-native";

interface ImageItem {
  id: string | null;
  imgPath: string | null;
}

interface Props {
  item: ImageItem;
  style: StyleProp<ImageStyle>;
}

const ItemThumbnail: React.FC<Props> = ({ item, style }: Props): ReactElement =>
  item.imgPath ? (
    <Image style={style} resizeMode="cover" source={{ uri: item.imgPath }} />
  ) : (
    <View style={[style, { backgroundColor: "#ccc" }]} />
  );

export default ItemThumbnail;
