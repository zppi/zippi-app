import React, { ReactElement } from "react";
import { Text, TextProps, TextStyle } from "react-native";
import { fonts, colors } from "../styles";

interface Props {
  bold?: boolean;
  italic?: boolean;
  title?: boolean;
  subtitle?: boolean;
}

const Txt: React.FC<Props & TextProps> = ({
  children,
  title,
  subtitle,
  bold,
  italic,
  style,
  ...rest
}): ReactElement => {
  const computedStyle: TextStyle = {
    fontFamily: fonts.default,
    color: colors.text
  };

  if (bold) {
    computedStyle.fontFamily = fonts.bold;
  }
  if (italic) {
    computedStyle.fontFamily = fonts.italic;
  }

  if (title) {
    computedStyle.fontSize = 25;
    computedStyle.fontFamily = fonts.bold;
  } else if (subtitle) {
    computedStyle.fontSize = 20;
  }

  return (
    <Text style={[computedStyle, style]} {...rest}>
      {children}
    </Text>
  );
};

export default Txt;
