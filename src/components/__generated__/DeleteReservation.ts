/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteReservation
// ====================================================

export interface DeleteReservation_deleteReservation {
  __typename: "DeleteReservationPayload";
  id: string;
  success: boolean;
  message: string;
}

export interface DeleteReservation {
  deleteReservation: DeleteReservation_deleteReservation;
}

export interface DeleteReservationVariables {
  id: string;
}
