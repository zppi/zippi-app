if (__DEV__) {
  const def = (name: string, val: any): void => ((global as any)[name] = val);

  def("test", "asdf");
  def("AuthService", require("./services/AuthService"));
  def("NavigationService", require("./navigation/NavigationService").default);
}
