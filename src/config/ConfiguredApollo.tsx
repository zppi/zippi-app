import ApolloClient, { InMemoryCache } from "apollo-boost";
import { AppLoading, Linking } from "expo";
import React, { ReactNode } from "react";
import { ApolloProvider } from "react-apollo";
import urlParser from "url-parse";
import { getSessionToken } from "../services/AuthService";
import { withDropdown } from "../navigation/DropdownContainer";

interface State {
  client: ApolloClient<InMemoryCache> | null;
}

export default class ConfiguredApollo extends React.Component<any, State> {
  state: State = { client: null };
  async componentWillMount(): Promise<void> {
    let uri = "https://zippi-server.herokuapp.com/graphql";

    if (__DEV__) {
      // In dev, assume the app server is running on the same machine
      // that hosts the expo server.
      const hostName = urlParser(Linking.makeUrl("")).hostname;
      uri = `http://${hostName}:2800/graphql`;
    }

    const client: ApolloClient<InMemoryCache> = new ApolloClient({
      uri,
      request: async operation => {
        const sessionToken = await getSessionToken();
        if (sessionToken) {
          operation.setContext({ headers: { Authorization: `Bearer ${sessionToken}` } });
        }
      },
      onError: ({ graphQLErrors, networkError }) => {
        let errors: string[] = [];
        if (graphQLErrors) {
          errors = [...errors, ...graphQLErrors.map(e => e.message as string)];
        }
        if (networkError) {
          errors = [...errors, networkError.message];
        }
        withDropdown(d => d.alertWithType("error", "Error", errors.join("\n")));
      }
    });

    this.setState({ client });
  }

  render(): ReactNode {
    if (this.state.client === null) {
      return <AppLoading />;
    }

    return <ApolloProvider client={this.state.client}>{this.props.children}</ApolloProvider>;
  }
}
