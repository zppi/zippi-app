import { ItemDetailsData_itemData_reservations } from "../screens/items/__generated__/ItemDetailsData";

export interface User {
  name: string;
}

export interface Item {
  id: string | null;
  name: string;
  description: string | null;
  reservations?: ItemDetailsData_itemData_reservations[] | null;
  rules?: string[];
  openTime: number | null;
  closeTime: number | null;
  imgPath: string | null;
  qrCode: string | null;
  groupId: string | null;
  maxReservationLength?: number | null;
  timeZone: string;
}

export interface Group {
  id: string;
  name: string;
}
