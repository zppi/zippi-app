import React, { ReactNode } from "react";
import { createAppContainer } from "react-navigation";
import authSwitchNavigator from "./authSwitchNavigator";
import { isSignedIn } from "../services/AuthService";
import { AppLoading } from "expo";
import NavigationService from "./NavigationService";
import DeepLinkHandler from "./DeepLinkHandler";
import { NavigationContainerComponent } from "react-navigation";
import { DropdownContainer } from "./DropdownContainer";

interface State {
  isSignedIn: boolean | null;
  navigatorReady: boolean;
}

export default class AppContainer extends React.Component<any, State> {
  state: State = { isSignedIn: null, navigatorReady: false };

  async componentWillMount(): Promise<void> {
    this.setState({ isSignedIn: await isSignedIn() });
  }

  setNavigator = (ref: NavigationContainerComponent): void => {
    NavigationService.setTopLevelNavigator(ref);
    if (!this.state.navigatorReady) {
      this.setState({ navigatorReady: true });
    }
  };

  render(): ReactNode {
    if (this.state.isSignedIn === null) {
      return <AppLoading />;
    }

    const NavigationAppContainer = createAppContainer(authSwitchNavigator(this.state.isSignedIn));

    return (
      <>
        <NavigationAppContainer ref={this.setNavigator} />
        {this.state.navigatorReady && <DeepLinkHandler />}
        <DropdownContainer />
      </>
    );
  }
}
