import { createStackNavigator } from "react-navigation";
import ScanCodeModal from "../screens/ScanCodeModal";
import TabNavigator from "./TabNavigator";
import routes from "./routes";
import NewReservation from "../screens/reservations/new/NewReservation";

export default createStackNavigator(
  {
    [routes.appTabs]: {
      screen: TabNavigator
    },
    [routes.scanCodeModal]: {
      screen: ScanCodeModal
    },
    [routes.newReservation]: {
      screen: NewReservation
    }
  },
  {
    mode: "modal",
    headerMode: "none"
  }
);
