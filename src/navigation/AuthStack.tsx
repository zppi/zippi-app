import { createStackNavigator } from "react-navigation";
import AwaitingConfirmationScreen from "../screens/login/AwaitingConfirmationScreen";
import LoginScreen from "../screens/login/LoginScreen";
import routes from "./routes";

export default createStackNavigator(
  {
    [routes.login]: LoginScreen,
    [routes.awaitingEmailConfirmation]: AwaitingConfirmationScreen
  },
  {
    headerMode: "none"
  }
);
