import createAnimatedSwitchNavigator from "react-navigation-animated-switch";
import AppStack from "./AppStack";
import AuthStack from "./AuthStack";
import routes from "./routes";
import { NavigationNavigator } from "react-navigation";

const authSwitchNavigator = (
  initializeAsAuthenticated: boolean
): NavigationNavigator<any, any, any> =>
  createAnimatedSwitchNavigator(
    {
      [routes.authRoot]: AuthStack,
      [routes.appRoot]: AppStack
    },
    { initialRouteName: initializeAsAuthenticated ? routes.appRoot : routes.authRoot }
  );

export default authSwitchNavigator;
