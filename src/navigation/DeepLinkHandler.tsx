import React, { ReactNode } from "react";
import gql from "graphql-tag";
import { Mutation, MutationFn } from "react-apollo";
import { Alert } from "react-native";
import { Linking } from "expo";
import { isSignedIn, signIn } from "../services/AuthService";
import {
  ExchangeSessionToken,
  ExchangeSessionTokenVariables
} from "./__generated__/ExchangeSessionToken";

let initialLinkHandled = false;

// Defines the mutation to exchange a verification token for a session token.
// When rendered, kicks off a
export default class DeepLinkHandler extends React.Component {
  verifyAuthToken: MutationFn | null = null;

  componentWillUnmount(): void {
    Linking.removeEventListener("url", this.handleDeepLink);
  }

  componentDidMount(): void {
    if (!initialLinkHandled) {
      Linking.getInitialURL().then((url: string) => this.handleDeepLink({ url }));
      initialLinkHandled = true;
    }

    Linking.removeEventListener("url", this.handleDeepLink); // clear out any dupe listeners
    Linking.addEventListener("url", this.handleDeepLink);
  }

  handleDeepLink = async ({ url }: { url: string }) => {
    const { path, queryParams } = Linking.parse(url);

    if (
      path === "emailVerified" &&
      "verificationToken" in queryParams &&
      !(await isSignedIn()) &&
      this.verifyAuthToken
    ) {
      this.verifyAuthToken({ variables: { verificationToken: queryParams.verificationToken } });
    }
  };

  finishSignIn = ({ exchangeSessionToken }: ExchangeSessionToken) => {
    signIn(exchangeSessionToken.sessionToken);
  };
  signInError = async () => {
    if (await isSignedIn()) {
      return; // Don't worry about it, probably a stray duplicate sign-in
    }
    Alert.alert(
      "Error signing in",
      "Sorry, we ran into an issue signing you into your account! Please try again. If the problem persists, let us know at help@zippi.app ASAP!"
    );
  };

  render(): ReactNode {
    return (
      <Mutation<ExchangeSessionToken, ExchangeSessionTokenVariables>
        mutation={gql`
          mutation ExchangeSessionToken($verificationToken: String!) {
            exchangeSessionToken(verificationToken: $verificationToken) {
              sessionToken
            }
          }
        `}
        onCompleted={this.finishSignIn}
        onError={this.signInError}
      >
        {verifyAuthToken => {
          this.verifyAuthToken = verifyAuthToken;
          return null;
        }}
      </Mutation>
    );
  }
}
