import React, { ReactElement } from "react";

import DropdownAlert from "react-native-dropdownalert";

let dropdownRef: DropdownAlert | null;

export const withDropdown = (f: (d: DropdownAlert) => any): void => dropdownRef && f(dropdownRef);

export const DropdownContainer = (): ReactElement => <DropdownAlert ref={r => (dropdownRef = r)} />;
