import { NavigationActions } from "react-navigation";
import routes from "./routes";
import { NavigationContainerComponent } from "react-navigation";

let topLevelNavigator: NavigationContainerComponent;

const setTopLevelNavigator = (navigatorRef: NavigationContainerComponent): void => {
  topLevelNavigator = navigatorRef;
};

const navigate = (routeName: string, params = {}): boolean =>
  topLevelNavigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );

export default {
  routes,
  setTopLevelNavigator,
  navigate
};
