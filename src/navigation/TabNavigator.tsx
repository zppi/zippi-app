import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import React, { ReactElement } from "react";
import { StyleSheet } from "react-native";
import { createBottomTabNavigator, createStackNavigator } from "react-navigation";
import AccountScreen from "../screens/account/AccountScreen";
import AllItems from "../screens/items/AllItems";
import ItemDetails from "../screens/items/ItemDetails";
import UpdateItem from "../screens/items/edit/UpdateItem";
import CreateItem from "../screens/items/create/CreateItem";
import SelectGroup from "../screens/items/create/SelectGroup";
import PreviewItem from "../screens/items/create/PreviewItem";
import AllReservations from "../screens/reservations/AllReservations";
import { colors, fonts } from "../styles";
import routes from "./routes";
import { NavigationBottomTabScreenOptions } from "react-navigation";

const ItemsStack = createStackNavigator({
  [routes.allItems]: AllItems,
  [routes.itemDetails]: ItemDetails,
  [routes.updateItem]: UpdateItem,
  [routes.createItem]: CreateItem,
  [routes.selectGroup]: SelectGroup,
  [routes.previewItem]: PreviewItem
});
ItemsStack.navigationOptions = ({ navigation }: any) => {
  let navigationOptions: any = { title: "Items" };
  let { routeName } = navigation.state.routes[navigation.state.index];
  if (routeName !== [routes.allItems][0] && routeName !== [routes.itemDetails][0]) {
    navigationOptions.tabBarVisible = false;
  }
  return navigationOptions;
};

const ReservationsStack = createStackNavigator({
  [routes.allReservations]: AllReservations
});
ReservationsStack.navigationOptions = { title: "Reservations" };

const styles = StyleSheet.create({
  tabBarStyle: {
    backgroundColor: "white",
    borderTopColor: "#999"
  },
  labelStyle: {
    textTransform: "uppercase",
    fontSize: 10,
    fontFamily: fonts.bold
  }
});

export default createBottomTabNavigator(
  {
    [routes.reservations]: ReservationsStack,
    [routes.items]: ItemsStack,
    [routes.account]: AccountScreen
  },
  {
    defaultNavigationOptions: ({ navigation }): NavigationBottomTabScreenOptions => ({
      tabBarIcon: ({ tintColor }): ReactElement => {
        const { routeName } = navigation.state;
        let iconName;
        let IconType = MaterialCommunityIcons;

        if (routeName === routes.reservations) {
          iconName = "ios-calendar";
          IconType = Ionicons;
        } else if (routeName === routes.items) {
          iconName = "layers-outline";
          IconType = MaterialCommunityIcons;
        } else if (routeName === routes.account) {
          iconName = "account-outline";
          IconType = MaterialCommunityIcons;
        }

        return <IconType name={iconName} size={30} color={tintColor} />;
      }
    }),
    initialRouteName: routes.items,
    tabBarOptions: {
      activeTintColor: colors.primary,
      inactiveTintColor: "#333",
      labelStyle: styles.labelStyle,
      style: styles.tabBarStyle
    }
  }
);

// export default TabNavigator;
