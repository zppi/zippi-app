/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ExchangeSessionToken
// ====================================================

export interface ExchangeSessionToken_exchangeSessionToken {
  __typename: "ExchangeSessionTokenPayload";
  sessionToken: string;
}

export interface ExchangeSessionToken {
  exchangeSessionToken: ExchangeSessionToken_exchangeSessionToken;
}

export interface ExchangeSessionTokenVariables {
  verificationToken: string;
}
