const routes = {
  authRoot: "root/authStack",
  appRoot: "root/appStack",

  login: "auth/login",
  awaitingEmailConfirmation: "auth/awaitingConfirmation",

  appTabs: "root/app/appTabs",

  // Reservations
  reservations: "tabs/reservations",
  allReservations: "tabs/reservations/all",
  newReservation: "modals/newReservation",

  // Items
  items: "tabs/items",
  allItems: "tabs/items/all",
  itemDetails: "tabs/items/details",
  updateItem: "tabs/items/edit/updateItem",
  createItem: "tabs/items/create/createItem",
  selectGroup: "tabs/items/create/selectGroup",
  createItemQR: "tabs/items/create/createItemQR",
  previewItem: "tabs/items/create/previewItem",

  // Account
  account: "tabs/account",

  // Modals
  scanCodeModal: "modals/scanCodeModal"
} as any;

export default routes;
