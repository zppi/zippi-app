import { Ionicons } from "@expo/vector-icons";
import * as Permissions from "expo-permissions";
import { BarCodeScanner } from "expo-barcode-scanner";
import React, { ReactNode } from "react";
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Txt } from "../components";
import { colors } from "../styles";

const overlayTextStyle = {
  color: "white",
  textShadowColor: "rgba(0, 0, 0, 0.75)",
  textShadowOffset: { width: -1, height: 1 },
  textShadowRadius: 10,
  padding: 10
};

const styles = StyleSheet.create({
  topContainer: { flex: 1, backgroundColor: "black" },
  alignmentContainer: {
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  alignmentBox: {
    borderWidth: 2,
    borderColor: "white",
    borderRadius: 20
  },
  container: { flex: 1 },
  titleLine: {
    marginTop: 30,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    overflow: "visible"
  },
  title: {
    ...overlayTextStyle
  },
  scannerIcon: {
    ...overlayTextStyle,
    fontSize: 30,
    marginTop: 4
  },
  backButton: {
    backgroundColor: colors.primary,
    padding: 20
  },
  backButtonText: {
    color: "white",
    textAlign: "center"
  }
});

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  hasCameraPermission: boolean | null;
  scanned: boolean;
}

export default class ScanCodeModal extends React.Component<Props, State> {
  state: State = {
    hasCameraPermission: null,
    scanned: false
  };

  async componentDidMount(): Promise<void> {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
  }

  render(): ReactNode {
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === false) {
      return (
        <SafeAreaView>
          <Txt title>No access to camera</Txt>
        </SafeAreaView>
      );
    }

    const dims = Dimensions.get("window");
    const windowSize = Math.min(dims.width, dims.height) * 0.8;

    return (
      <View style={styles.topContainer}>
        <StatusBar barStyle="light-content" />
        {hasCameraPermission && (
          <BarCodeScanner
            onBarCodeScanned={this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />
        )}
        <View style={[StyleSheet.absoluteFillObject, styles.alignmentContainer]}>
          <View style={styles.alignmentBox}>
            <View style={{ width: windowSize, height: windowSize }} />
          </View>
        </View>
        <SafeAreaView style={styles.container}>
          <View style={styles.titleLine}>
            <Ionicons name="md-qr-scanner" style={styles.scannerIcon} />
            <Txt title style={styles.title}>
              Scan a QR Code
            </Txt>
          </View>

          <View style={{ flex: 1 }} />
          <TouchableOpacity
            activeOpacity={0.9}
            style={styles.backButton}
            onPress={() => this.props.navigation.goBack()}
          >
            <Txt subtitle style={styles.backButtonText}>
              Cancel
            </Txt>
          </TouchableOpacity>
        </SafeAreaView>
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }: { type: string; data: string }) => {
    if (this.state.scanned) {
      return;
    }
    this.setState({ scanned: true });
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };
}
