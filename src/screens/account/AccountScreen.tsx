import React, { ReactNode } from "react";
import { Text, View, Button } from "react-native";
import { signOut } from "../../services/AuthService";

export default class AccountScreen extends React.Component {
  static navigationOptions = {
    title: "Account"
  };

  render(): ReactNode {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Account</Text>
        <Button onPress={signOut} title="Sign Out" />
      </View>
    );
  }
}
