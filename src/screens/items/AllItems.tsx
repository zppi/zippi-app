import { chunk } from "lodash";
import React, { Component, ReactNode, ReactElement } from "react";
import { SafeAreaView, StyleSheet, View, RefreshControl } from "react-native";
import ActionButton from "react-native-action-button";
import TouchableScale from "react-native-touchable-scale";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import { NavigationScreenProp, NavigationStackScreenOptions, SectionList } from "react-navigation";
import { Txt, ItemThumbnail } from "../../components";
import { colors, containers, setElevation, abIcon } from "../../styles";
import routes from "../../navigation/routes";
import { graphql, ChildProps } from "react-apollo";
import gql from "graphql-tag";
import { flatten } from "lodash";
import { AllItemsQuery_myGroups_items, AllItemsQuery } from "./__generated__/AllItemsQuery";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

type Row = AllItemsQuery_myGroups_items[];
interface Section {
  title: string;
  data: Row[];
}

// Tried to follow example of https://github.com/benawad/apollo-codegen-example-usage/blob/master/src/components/User1.tsx
// but can't make types work for this query.
// @ts-ignore
@graphql(gql`
  query AllItemsQuery {
    myGroups {
      id
      name

      items {
        id
        name
        hasReserved
        imgPath
      }
    }
  }
`)
export default class AllItems extends Component<ChildProps<Props, AllItemsQuery>> {
  static navigationOptions: NavigationStackScreenOptions = {
    header: null,
    headerBackTitle: null
  };
  focusListener: any;

  componentDidMount(): void {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => this.onFocusing());
  }

  componentWillUnmount(): void {
    this.focusListener.remove();
  }

  onFocusing(): void {
    let navigateItemId = null;
    if (this.props.navigation.state.params) {
      navigateItemId = this.props.navigation.state.params.itemId;
    }
    this.props.navigation.setParams({ itemId: null });
    if (navigateItemId) {
      this.props.navigation.navigate(routes.itemDetails, {
        id: navigateItemId
      });
      this.props.data && this.props.data.refetch();
    }
  }

  renderSectionHeader = ({ section: { title } }: { section: Section }): ReactElement => (
    <Txt subtitle style={{ paddingBottom: 10, paddingTop: 15 }}>
      {title}
    </Txt>
  );

  renderRow = ({ item }: { item: Row }) => (
    <View style={styles.rowWrapper}>
      {this.renderItem(item[0])}
      {item[1] && <View style={{ width: 10 }} />}
      {item[1] && this.renderItem(item[1])}
    </View>
  );

  renderItem = (item: AllItemsQuery_myGroups_items): ReactNode => (
    <TouchableScale
      activeScale={0.95}
      style={styles.itemShadowWrapper}
      tension={250}
      friction={20}
      onPress={(): boolean => this.props.navigation.navigate(routes.itemDetails, { id: item.id })}
    >
      <View style={styles.itemContainer}>
        <ItemThumbnail style={styles.itemImage} item={item} />
        <View style={styles.itemNameWrapper}>
          <Txt style={styles.itemName} numberOfLines={2}>
            {item.name}
          </Txt>
        </View>
      </View>
    </TouchableScale>
  );

  render(): ReactNode {
    if (!this.props.data || !this.props.data.myGroups) {
      return null;
    }

    const myItems: Section = {
      title: "Items I've Reserved",
      data: chunk(flatten(this.props.data.myGroups.map(g => g.items.filter(i => i.hasReserved))), 2)
    };

    const sections: Section[] = this.props.data.myGroups.map(g => ({
      title: g.name,
      data: chunk(g.items.filter(i => !i.hasReserved), 2)
    }));

    const ItemList: SectionList<Row> = SectionList;

    return (
      <SafeAreaView style={{ height: "100%" }}>
        <ItemList
          refreshControl={
            <RefreshControl
              refreshing={this.props.data.networkStatus === 4}
              onRefresh={() => this.props.data && this.props.data.refetch()}
            />
          }
          contentContainerStyle={containers.whiteBackground}
          sections={[myItems, ...sections]}
          renderSectionHeader={this.renderSectionHeader}
          renderItem={this.renderRow}
          keyExtractor={(twoItems): string => twoItems[0].id}
          ListHeaderComponent={
            <Txt title style={{ paddingBottom: 18 }}>
              Items
            </Txt>
          }
          stickySectionHeadersEnabled={false}
        />
        <ActionButton buttonColor={colors.darkBlue} fixNativeFeedbackRadius>
          <ActionButton.Item
            buttonColor="#9b59b6"
            title="Create New Item"
            onPress={() => this.props.navigation.navigate(routes.createItem)}
          >
            <MaterialCommunityIcons name="wrench" style={abIcon} />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="#3498db"
            title="Add Item"
            onPress={() => this.props.navigation.navigate(routes.scanCodeModal)}
          >
            <MaterialCommunityIcons name="layers-outline" style={abIcon} />
          </ActionButton.Item>
        </ActionButton>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  rowWrapper: {
    flexDirection: "row",
    marginBottom: 20,
    justifyContent: "space-between"
  },
  itemShadowWrapper: {
    flex: 1,
    ...setElevation(10)
  },
  itemContainer: {
    backgroundColor: "white",
    borderRadius: 5,
    overflow: "hidden",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#999"
  },
  itemImage: {
    width: "100%",
    aspectRatio: 2 / 1
  },
  itemNameWrapper: {
    paddingHorizontal: 8,
    height: 45,
    alignItems: "center",
    justifyContent: "center"
  },
  itemName: {
    textAlign: "center"
  }
});
