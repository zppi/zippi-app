import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import React, { ReactNode } from "react";
import { StatusBar, StyleSheet, View, RefreshControl, TouchableOpacity } from "react-native";
import ActionButton from "react-native-action-button";
import { NavigationScreenProp, ScrollView } from "react-navigation";

import { colors, hitSlop } from "../../styles";
import routes from "../../navigation/routes";
import { graphql, ChildProps } from "react-apollo";
import gql from "graphql-tag";
import { ItemDetailsData, ItemDetailsDataVariables } from "./__generated__/ItemDetailsData";
import { DisplayItem } from "../../components";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

class ItemDetails extends React.Component<
  ChildProps<Props, ItemDetailsData, ItemDetailsDataVariables>
> {
  static navigationOptions: any = ({ navigation }: any) => {
    return {
      headerTransparent: true,
      headerTintColor: "white",
      headerRight: (
        <TouchableOpacity hitSlop={hitSlop.large} onPress={navigation.getParam("edit")}>
          <MaterialCommunityIcons
            name="lead-pencil"
            style={{ fontSize: 30, color: "white", marginRight: 20 }}
          />
        </TouchableOpacity>
      )
    };
  };

  componentDidMount(): void {
    this.props.navigation.setParams({
      edit: () =>
        this.props.navigation.navigate(routes.updateItem, {
          id: this.props.navigation.getParam("id")
        })
    });
  }

  render(): ReactNode {
    const { data } = this.props;

    if (!data || !data.itemData || data.error) {
      return null;
    }

    return (
      <>
        <View style={[StyleSheet.absoluteFill, { backgroundColor: "white" }]}>
          <View style={{ height: "50%", backgroundColor: "black" }} />
        </View>
        <StatusBar barStyle="light-content" />
        <ScrollView
          contentContainerStyle={{ backgroundColor: "white", paddingBottom: 80 }}
          refreshControl={
            <RefreshControl refreshing={data.networkStatus === 4} onRefresh={data.refetch} />
          }
        >
          <DisplayItem item={{ ...data.itemData, groupId: data.itemData.group.id }} />
        </ScrollView>
        <LinearGradient
          colors={["rgba(0, 0, 0, 0.4)", "transparent"]}
          style={styles.headerShadow}
        />

        <ActionButton
          buttonColor={colors.darkBlue}
          fixNativeFeedbackRadius
          onPress={() =>
            data.itemData &&
            this.props.navigation.navigate(routes.newReservation, {
              itemId: data.itemData.id,
              timeZone: data.itemData.timeZone
            })
          }
          renderIcon={() => <MaterialCommunityIcons name="calendar-plus" style={styles.abIcon} />}
        />
      </>
    );
  }
}

export const ITEM_DETAILS_QUERY = gql`
  query ItemDetailsData($id: String!) {
    itemData(id: $id) {
      id
      name
      description
      imgPath
      qrCode
      openTime
      closeTime
      maxReservationLength
      timeZone
      group {
        id
      }
      rules
      reservations {
        id
        startTime
        endTime
        canDelete
        user {
          id
          email
        }
      }
    }
  }
`;

export default graphql<Props, ItemDetailsData, ItemDetailsDataVariables>(ITEM_DETAILS_QUERY, {
  options: props => ({
    variables: { id: props.navigation.getParam("id"), after: new Date().toISOString() }
  })
})(ItemDetails);

const styles = StyleSheet.create({
  headerShadow: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 80
  },
  abIcon: {
    color: "white",
    fontSize: 30,
    marginTop: 6
  }
});
