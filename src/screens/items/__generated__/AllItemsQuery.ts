/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AllItemsQuery
// ====================================================

export interface AllItemsQuery_myGroups_items {
  __typename: "Item";
  id: string;
  name: string;
  hasReserved: boolean;
  imgPath: string | null;
}

export interface AllItemsQuery_myGroups {
  __typename: "Group";
  id: string;
  name: string;
  items: AllItemsQuery_myGroups_items[];
}

export interface AllItemsQuery {
  myGroups: AllItemsQuery_myGroups[] | null;
}
