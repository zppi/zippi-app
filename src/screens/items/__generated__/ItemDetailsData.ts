/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ItemDetailsData
// ====================================================

export interface ItemDetailsData_itemData_group {
  __typename: "Group";
  id: string;
}

export interface ItemDetailsData_itemData_reservations_user {
  __typename: "UserData";
  id: string;
  email: string;
}

export interface ItemDetailsData_itemData_reservations {
  __typename: "Reservation";
  id: string;
  startTime: any;
  endTime: any;
  canDelete: boolean;
  user: ItemDetailsData_itemData_reservations_user;
}

export interface ItemDetailsData_itemData {
  __typename: "Item";
  id: string;
  name: string;
  description: string | null;
  imgPath: string | null;
  qrCode: string;
  openTime: number | null;
  closeTime: number | null;
  maxReservationLength: number | null;
  timeZone: string;
  group: ItemDetailsData_itemData_group;
  rules: string[];
  reservations: ItemDetailsData_itemData_reservations[];
}

export interface ItemDetailsData {
  itemData: ItemDetailsData_itemData | null;
}

export interface ItemDetailsDataVariables {
  id: string;
}
