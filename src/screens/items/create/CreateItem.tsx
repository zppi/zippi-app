import React, { ReactNode } from "react";
import { StyleSheet, TouchableOpacity, Platform } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Permissions } from "expo";
import * as Localization from "expo-localization";

import { Txt, EditItem } from "../../../components";
import { Item } from "../../../models/models";
import routes from "../../../navigation/routes";
import { hitSlop, colors } from "../../../styles";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  name: string;
  description: string;
  imgPath: string | null;
  rules: string[];
  openTime: number | null;
  closeTime: number | null;
  maxReservationLength: number | null;
}

export default class CreateItem extends React.Component<Props, State> {
  static navigationOptions: any = ({ navigation }: any) => {
    const rightDisabled = !navigation.getParam("canContinue");
    return {
      headerStyle: {
        backgroundColor: "rgba(0,0,0,0.5)"
      },
      headerTransparent: true,
      headerTintColor: "white",
      headerRight: (
        <TouchableOpacity
          disabled={rightDisabled}
          hitSlop={hitSlop.large}
          onPress={navigation.getParam("selectGroup")}
        >
          <Txt style={rightDisabled ? styles.textDisabled : styles.textEnabled}>CONTINUE</Txt>
        </TouchableOpacity>
      )
    };
  };

  componentDidMount(): void {
    this.getPermissionAsync();
    this.props.navigation.setParams({
      selectGroup: this.continue,
      canContinue: false
    });
  }

  state: State = {
    name: "",
    description: "",
    imgPath: null,
    rules: [],
    openTime: null,
    closeTime: null,
    maxReservationLength: null
  };

  continue = (): void => {
    let editedRules: string[] = [];
    this.state.rules.forEach((rule: string) => {
      /\S/.test(rule) && editedRules.push(rule);
    });
    const item: Item = {
      id: null,
      name: this.state.name,
      description: this.state.description,
      imgPath: this.state.imgPath,
      groupId: null,
      qrCode: null,
      openTime: this.state.openTime,
      closeTime: this.state.closeTime,
      rules: editedRules,
      maxReservationLength: this.state.maxReservationLength,
      timeZone: Localization.timezone
    };
    this.props.navigation.navigate(routes.selectGroup, { item: item });
  };

  timeOkay(): boolean {
    if (this.state.openTime && this.state.closeTime) {
      return this.state.openTime < this.state.closeTime;
    }
    return true;
  }

  updateCanContinue(): void {
    const canContinue = this.state.name !== "" && this.timeOkay();
    this.props.navigation.setParams({
      canContinue: canContinue
    });
  }

  updateName = (name: string): void => {
    this.setState({ name }, () => {
      this.updateCanContinue();
    });
  };

  updateDescription = (description: string): void => {
    this.setState({ description });
  };

  updateImage = (imgPath: string) => {
    this.setState({ imgPath });
  };

  updateOpenTime = (openTime: number) => {
    this.setState({ openTime }, () => {
      this.updateCanContinue();
    });
  };

  updateCloseTime = (closeTime: number) => {
    this.setState({ closeTime }, () => {
      this.updateCanContinue();
    });
  };

  updateRules = (rules: string[]) => {
    this.setState({ rules });
  };

  addRule = (rule: string) => {
    let rules: string[] = this.state.rules;
    rules.push(rule);
    this.setState({ rules });
  };

  deleteRule = (index: number) => {
    let rules: string[] = this.state.rules;
    rules.splice(index, 1);
    this.setState({ rules });
  };

  updateMaxReservationLength = (maxLength: number) => {
    this.setState({ maxReservationLength: maxLength });
  };

  getPermissionAsync = async () => {
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  render(): ReactNode {
    const item = {
      __typename: "Item",
      name: this.state.name,
      description: this.state.description,
      reservations: null,
      imgPath: this.state.imgPath,
      rules: this.state.rules,
      openTime: this.state.openTime,
      closeTime: this.state.closeTime,
      timeZone: Localization.timezone,
      id: null,
      qrCode: null,
      groupId: null,
      maxReservationLength: this.state.maxReservationLength
    };
    return (
      <EditItem
        item={item}
        isUploading={false}
        updateName={this.updateName}
        updateDescription={this.updateDescription}
        updateImage={this.updateImage}
        updateOpenTime={this.updateOpenTime}
        updateCloseTime={this.updateCloseTime}
        updateRules={this.updateRules}
        updateMaxReservationLength={this.updateMaxReservationLength}
      />
    );
  }
}

const text: any = {
  textAlign: "center",
  fontSize: 20,
  padding: 10
};

const styles = StyleSheet.create({
  textEnabled: {
    color: "white",
    ...text
  },
  textDisabled: {
    color: colors.darkGray,
    ...text
  }
});
