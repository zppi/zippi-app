import React, { ReactNode } from "react";
import { StatusBar, StyleSheet, View, TouchableOpacity, ActivityIndicator } from "react-native";
import { NavigationScreenProp, ScrollView } from "react-navigation";
import { Linking } from "expo";
import urlParser from "url-parse";
import { Mutation } from "react-apollo";

import { DisplayItem, Txt } from "../../../components";
import { colors, hitSlop } from "../../../styles";
import routes from "../../../navigation/routes";
import gql from "graphql-tag";
import { CreateItem, CreateItemVariables } from "./__generated__/CreateItem";
import { getSessionToken } from "../../../services/AuthService";
import { Item } from "../../../models/models";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  sessionToken: string | null;
  uploading: boolean;
}

const CREATE_ITEM = gql`
  mutation CreateItem(
    $name: String!
    $description: String
    $groupName: String
    $groupId: String
    $qrCode: String
    $openTime: Int
    $closeTime: Int
    $timeZone: String
    $rules: [String!]
    $maxReservationLength: Int
  ) {
    createItem(
      name: $name
      description: $description
      groupName: $groupName
      groupId: $groupId
      qrCode: $qrCode
      openTime: $openTime
      closeTime: $closeTime
      timeZone: $timeZone
      rules: $rules
      maxReservationLength: $maxReservationLength
    ) {
      id
      name
      description
    }
  }
`;

export default class PreviewItem extends React.Component<Props, State> {
  static navigationOptions: any = ({ navigation }: any) => {
    const item = navigation.getParam("item") || { id: null, name: null, description: null };
    const onSubmitCompleted = navigation.getParam("onSubmitCompleted");
    const mutation = (
      <Mutation<CreateItem, CreateItemVariables>
        mutation={CREATE_ITEM}
        onCompleted={result => onSubmitCompleted(result)}
        variables={{
          name: item.name,
          description: item.description,
          groupName: navigation.getParam("groupName"),
          groupId: item.groupId,
          qrCode: item.qrCode,
          openTime: item.openTime,
          closeTime: item.closeTime,
          timeZone: item.timeZone,
          rules: item.rules,
          maxReservationLength: item.maxReservationLength
        }}
      >
        {createItem => (
          <TouchableOpacity hitSlop={hitSlop.large} onPress={() => createItem()}>
            <Txt style={styles.textEnabled}>CREATE</Txt>
          </TouchableOpacity>
        )}
      </Mutation>
    );
    return {
      headerStyle: {
        backgroundColor: "rgba(0,0,0,0.5)"
      },
      headerTransparent: true,
      headerTintColor: "white",
      headerRight: navigation.getParam("uploadingIndicator") || mutation
    };
  };

  async componentDidMount(): Promise<void> {
    const sessionToken = await getSessionToken();
    this.setState({ sessionToken: sessionToken });
    const item: Item = this.props.navigation.getParam("item");
    this.props.navigation.setParams({
      item: item,
      onSubmitCompleted: (result: any) => this.onSubmitCompleted(result)
    });
  }

  state = { sessionToken: "", uploading: false };

  onSubmitCompleted = (result: any) => {
    const item: Item = this.props.navigation.getParam("item");

    if (result.createItem && result.createItem.id && item.imgPath) {
      this.uploadPicture(item.imgPath, result.createItem.id);
    } else {
      this.props.navigation.navigate(routes.allItems, { itemId: result.createItem.id });
    }
  };

  uploadPicture = async (imageURI: string, itemId: string) => {
    if (imageURI) {
      const data = new FormData();
      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data;",
          Authorization: `Bearer ${this.state.sessionToken}`,
          "X-Item-Id": itemId
        },
        body: data
      };

      const hostName = urlParser(Linking.makeUrl("")).hostname;

      data.append("picture", {
        // @ts-ignore
        uri: imageURI,
        name: "selfie.jpg",
        type: "image/jpg"
      });
      this.setUploading(true);

      await fetch(`http://${hostName}:2800/item_image`, config);

      this.setUploading(false);
      this.props.navigation.navigate(routes.allItems, { itemId: itemId });
    }
  };

  setUploading(uploading: boolean): void {
    this.setState({ uploading });
    const uploadingIndicator = uploading ? (
      <View>
        <ActivityIndicator style={{ padding: 10 }} size="large" color={colors.primary} />
      </View>
    ) : null;
    this.props.navigation.setParams({
      uploadingIndicator
    });
  }

  render(): ReactNode {
    const item: Item = this.props.navigation.getParam("item");

    return (
      <>
        <View style={[StyleSheet.absoluteFill, { backgroundColor: "white" }]}>
          <View style={{ height: "50%", backgroundColor: "black" }} />
        </View>
        <StatusBar barStyle="light-content" />
        <ScrollView contentContainerStyle={{ backgroundColor: "white", paddingBottom: 20 }}>
          <DisplayItem item={item} />
          <View style={styles.qrSection}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate(routes.scanCodeModal)}
              style={styles.qrButton}
            >
              <Txt subtitle style={styles.qrButtonText}>
                Scan Zippi Sticker
              </Txt>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </>
    );
  }
}

const text: any = {
  textAlign: "center",
  fontSize: 20,
  padding: 10
};

const styles = StyleSheet.create({
  qrSection: {
    alignItems: "center"
  },
  qrButton: {
    padding: 20,
    margin: 10,
    borderRadius: 15,
    backgroundColor: colors.primary
  },
  qrButtonText: {
    color: "white"
  },
  textEnabled: {
    color: "white",
    ...text
  }
});
