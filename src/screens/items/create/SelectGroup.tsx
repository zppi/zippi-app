import React, { ReactNode } from "react";
import {
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  TextInput,
  Modal
} from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { graphql, ChildProps } from "react-apollo";
import gql from "graphql-tag";

import { hitSlop, colors } from "../../../styles";
import { Txt, BottomButtons } from "../../../components";
import { Item, Group } from "../../../models/models";
import routes from "../../../navigation/routes";
import { MyGroups } from "./__generated__/MyGroups";

interface Props {
  navigation: NavigationScreenProp<any, any>;
  item: Item;
}

interface State {
  groupId: string;
  groupName: string;
  groups: Group[];
  modalVisible: boolean;
}
// @ts-ignore
@graphql(gql`
  query MyGroups {
    myGroups {
      id
      name
    }
  }
`)
export default class SelectGroup extends React.Component<ChildProps<Props, MyGroups>, State> {
  state = {
    groupId: "",
    groupName: "",
    groups: [],
    modalVisible: false
  };

  componentDidMount(): void {
    this.setState({ groups: this.formatGroups() });
  }

  compareGroups(a: Group, b: Group): number {
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1;
    }
    return 1;
  }

  formatGroups(): Group[] {
    if (this.props.data && this.props.data.myGroups) {
      let formattedGroups: Group[] = [];
      this.props.data.myGroups.forEach(group => {
        formattedGroups.push({ id: group.id, name: group.name });
      });
      return formattedGroups.sort(this.compareGroups);
    }
    return [];
  }

  setModalVisible = (visible: boolean) => this.setState({ modalVisible: visible });

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginRight: "14%"
        }}
      />
    );
  };

  select(id: string): void {
    let item = this.props.navigation.getParam("item");
    item.groupId = id;
    this.props.navigation.navigate(routes.previewItem, { item });
  }

  createGroup(name: string): void {
    this.setModalVisible(false);
    let item = this.props.navigation.getParam("item");
    this.props.navigation.navigate(routes.previewItem, { item, groupName: name });
  }

  onNameChange(name: string): void {
    this.setState({ groupName: name });
  }

  render(): ReactNode {
    const item = this.props.navigation.getParam("item");
    return (
      <>
        <SafeAreaView style={styles.content}>
          <Txt title style={styles.title}>
            Choose a group for {item.name}
          </Txt>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            presentationStyle="overFullScreen"
            onRequestClose={() => this.setModalVisible(false)}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "rgba(0,0,0,0.5)"
              }}
            >
              <View
                style={{
                  width: "90%",
                  height: 200,
                  backgroundColor: "white",
                  alignItems: "center",
                  justifyContent: "space-between"
                }}
              >
                <Txt title>Create your group</Txt>
                <TextInput
                  style={styles.input}
                  placeholder="New Group Name"
                  onChangeText={text => this.onNameChange(text)}
                  value={this.state.groupName}
                />
                <BottomButtons
                  buttons={[
                    { text: "CANCEL", action: () => this.setModalVisible(false) },
                    {
                      text: "CONTINUE",
                      action: () => this.createGroup(this.state.groupName),
                      disabled: this.state.groupName === ""
                    }
                  ]}
                  showOnKeyboard={true}
                />
              </View>
            </View>
          </Modal>
          {this.state.groups.length > 0 && (
            <View style={styles.listSection}>
              <Txt subtitle>Select a group</Txt>
              <FlatList
                style={styles.listContainer}
                data={this.state.groups}
                renderItem={({ item }: any) => (
                  <TouchableOpacity
                    style={styles.groupSelector}
                    hitSlop={hitSlop.large}
                    onPress={() => this.select(item.id)}
                  >
                    <Txt>{item.name}</Txt>
                  </TouchableOpacity>
                )}
                keyExtractor={(item: any) => item.id}
                ItemSeparatorComponent={this.renderSeparator}
                extraData={this.state}
              />
            </View>
          )}
          <View style={styles.createSection}>
            <TouchableOpacity
              style={styles.createButton}
              onPress={() => {
                this.setModalVisible(true);
              }}
            >
              <Txt title style={styles.buttonText}>
                Create new group +
              </Txt>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    alignItems: "center",
    padding: "10%"
  },
  title: {
    marginBottom: 40,
    marginTop: 20
  },
  subtitle: {
    marginTop: 10,
    marginBottom: 10
  },
  listSection: {
    width: "100%",
    maxHeight: "60%"
  },
  listContainer: {
    marginTop: "5%",
    borderColor: colors.lightGray,
    borderTopWidth: 5,
    borderBottomWidth: 5
  },
  groupSelector: {
    width: "100%",
    flex: 1,
    height: 28,
    margin: 7,
    padding: 7
  },
  createSection: {
    width: "100%",
    paddingTop: "10%",
    alignContent: "center"
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderBottomWidth: 1,
    width: "80%"
  },
  createButton: {
    backgroundColor: colors.darkBlue,
    padding: 20,
    alignItems: "center",
    borderRadius: 10
  },
  buttonText: {
    color: "white",
    fontSize: 20
  }
});
