/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateItem
// ====================================================

export interface CreateItem_createItem {
  __typename: "Item";
  id: string;
  name: string;
  description: string | null;
}

export interface CreateItem {
  createItem: CreateItem_createItem;
}

export interface CreateItemVariables {
  name: string;
  description?: string | null;
  groupName?: string | null;
  groupId?: string | null;
  qrCode?: string | null;
  openTime?: number | null;
  closeTime?: number | null;
  timeZone?: string | null;
  rules?: string[] | null;
  maxReservationLength?: number | null;
}
