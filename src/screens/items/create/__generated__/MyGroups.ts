/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MyGroups
// ====================================================

export interface MyGroups_myGroups {
  __typename: "Group";
  id: string;
  name: string;
}

export interface MyGroups {
  myGroups: MyGroups_myGroups[] | null;
}
