import React, { ReactNode } from "react";
import { StyleSheet, View, TouchableOpacity, ActivityIndicator, Platform } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Mutation } from "react-apollo";
import { Linking } from "expo";
import urlParser from "url-parse";

import * as Permissions from "expo-permissions";
import { Txt, EditItem } from "../../../components";
import { hitSlop, colors } from "../../../styles";
import { graphql, ChildProps } from "react-apollo";
import gql from "graphql-tag";
import { UpdateItemData, UpdateItemDataVariables } from "./__generated__/UpdateItemData";
import {
  UpdateItemMutation,
  UpdateItemMutationVariables
} from "./__generated__/UpdateItemMutation";
import { getSessionToken } from "../../../services/AuthService";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  name: string | null;
  description: string | null;
  imgPath: string | null;
  rules: string[] | null;
  openTime: number | null;
  closeTime: number | null;
  maxReservationLength: number | null;
  sessionToken: "";
  uploading: boolean;
}

const UPDATE_ITEM = gql`
  mutation UpdateItemMutation(
    $id: String!
    $name: String
    $description: String
    $openTime: Int
    $closeTime: Int
    $rules: [String!]
    $maxReservationLength: Int
  ) {
    updateItem(
      id: $id
      name: $name
      description: $description
      openTime: $openTime
      closeTime: $closeTime
      rules: $rules
      maxReservationLength: $maxReservationLength
    ) {
      id
      name
      description
      openTime
      closeTime
    }
  }
`;

class UpdateItem extends React.Component<
  ChildProps<Props, UpdateItemData, UpdateItemDataVariables>
> {
  static navigationOptions: any = ({ navigation }: any) => {
    const item = navigation.getParam("item") || { id: null, name: null, description: null };
    const mutation = (
      <Mutation<UpdateItemMutation, UpdateItemMutationVariables>
        mutation={UPDATE_ITEM}
        onCompleted={navigation.getParam("onSubmitCompleted")}
        variables={{
          id: item.id,
          name: item.name,
          description: item.description,
          openTime: item.openTime,
          closeTime: item.closeTime,
          rules: item.rules,
          maxReservationLength: item.maxReservationLength
        }}
      >
        {updateMutation => {
          return (
            <TouchableOpacity
              hitSlop={hitSlop.large}
              onPress={() => updateMutation()}
              disabled={!navigation.getParam("canSave")}
            >
              <Txt
                style={navigation.getParam("canSave") ? styles.textEnabled : styles.textDisabled}
              >
                SAVE
              </Txt>
            </TouchableOpacity>
          );
        }}
      </Mutation>
    );
    return {
      headerStyle: {
        backgroundColor: "rgba(0,0,0,0.5)"
      },
      headerTransparent: true,
      headerTintColor: "white",
      headerRight: navigation.getParam("uploadingIndicator") || mutation
    };
  };

  state: State = {
    name: null,
    description: null,
    imgPath: null,
    rules: null,
    openTime: null,
    closeTime: null,
    maxReservationLength: null,
    sessionToken: "",
    uploading: false
  };

  onSubmitCompleted = (): void => {
    const data = this.props.data;
    if (
      data &&
      data.itemData &&
      this.state.imgPath &&
      data.itemData.imgPath !== this.state.imgPath
    ) {
      this.uploadPicture();
    } else {
      this.props.navigation.goBack();
    }
  };

  componentDidMount(): void {
    this.props.navigation.setParams({
      onSubmitCompleted: () => this.onSubmitCompleted()
    });
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    const sessionToken = await getSessionToken();
    this.setState({ sessionToken: sessionToken });
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  uploadPicture = async () => {
    if (!this.props.data || !this.props.data.itemData) return;

    const data = new FormData();
    const config = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data;",
        Authorization: `Bearer ${this.state.sessionToken}`,
        "X-Item-Id": this.props.data.itemData.id
      },
      body: data
    };

    const hostName = urlParser(Linking.makeUrl("")).hostname;

    data.append("picture", {
      // @ts-ignore
      uri: this.state.imgPath,
      name: "selfie.jpg",
      type: "image/jpg"
    });
    this.setUploading(true);

    await fetch(`http://${hostName}:2800/item_image`, config);

    const response = await this.props.data.refetch();
    response.data &&
      response.data.itemData &&
      this.setState({ imgPath: response.data.itemData.imgPath });
    this.setUploading(false);
    this.props.navigation.goBack();
  };

  secondsFromMidnight(date: Date): number {
    return date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
  }

  timeOkay(openTime: number | null, closeTime: number | null): boolean {
    if (openTime && closeTime) {
      return openTime < closeTime;
    }
    return true;
  }

  pruneRules(): string[] | null {
    if (!this.state.rules) return null;
    let editedRules: string[] = [];
    this.state.rules.forEach((rule: string) => {
      /\S/.test(rule) && editedRules.push(rule);
    });
    return editedRules;
  }

  updateItem(): void {
    if (!this.props.data || !this.props.data.itemData) return;
    const propsItem = this.props.data.itemData;
    const openTime = this.state.openTime !== null ? this.state.openTime : propsItem.openTime;
    const closeTime = this.state.closeTime !== null ? this.state.closeTime : propsItem.closeTime;
    const maxReservationLength =
      this.state.maxReservationLength !== null
        ? this.state.maxReservationLength
        : propsItem.maxReservationLength;
    const canSave = this.state.name !== "" && this.timeOkay(openTime, closeTime);
    let editedRules = this.pruneRules();
    const item = {
      id: propsItem.id,
      name: this.state.name,
      description: this.state.description,
      openTime: openTime,
      closeTime: closeTime,
      rules: editedRules,
      maxReservationLength: maxReservationLength
    };
    this.props.navigation.setParams({
      canSave: canSave,
      item: item
    });
  }

  updateName = (name: string): void => {
    this.setState({ name }, () => {
      this.updateItem();
    });
  };

  updateDescription = (description: string): void => {
    this.setState({ description }, () => {
      this.updateItem();
    });
  };

  updateImage = (imgPath: string) => {
    this.setState({ imgPath });
  };

  updateOpenTime = (openTime: number) => {
    this.setState({ openTime }, () => {
      this.updateItem();
    });
  };

  updateCloseTime = (closeTime: number) => {
    this.setState({ closeTime }, () => {
      this.updateItem();
    });
  };

  updateRules = (rules: string[]) => {
    this.setState({ rules }, () => {
      this.updateItem();
    });
  };

  updateMaxReservationLength = (maxLength: number | null) => {
    this.setState({ maxReservationLength: maxLength }, () => {
      this.updateItem();
    });
  };

  setUploading(uploading: boolean): void {
    this.setState({ uploading });
    const uploadingIndicator = uploading ? (
      <View>
        <ActivityIndicator style={{ padding: 10 }} size="large" color={colors.primary} />
      </View>
    ) : null;
    this.props.navigation.setParams({
      uploadingIndicator
    });
  }

  render(): ReactNode {
    if (!this.props.data || !this.props.data.itemData) {
      return null;
    }
    const propsItem = this.props.data.itemData;
    const openTime = this.state.openTime !== null ? this.state.openTime : propsItem.openTime;
    const closeTime = this.state.closeTime !== null ? this.state.closeTime : propsItem.closeTime;
    const maxReservationLength =
      this.state.maxReservationLength !== null
        ? this.state.maxReservationLength
        : propsItem.maxReservationLength;
    const item = {
      __typename: "Item",
      name: this.state.name || propsItem.name,
      description: this.state.description || propsItem.description,
      imgPath: this.state.imgPath || propsItem.imgPath,
      rules: this.state.rules || propsItem.rules,
      openTime: openTime,
      closeTime: closeTime,
      maxReservationLength: maxReservationLength,
      timeZone: this.props.data.itemData.timeZone || propsItem.timeZone,
      id: propsItem.id,
      qrCode: null,
      groupId: null
    };

    return (
      <EditItem
        item={item}
        isUploading={this.state.uploading}
        updateName={this.updateName}
        updateDescription={this.updateDescription}
        updateImage={this.updateImage}
        updateOpenTime={this.updateOpenTime}
        updateCloseTime={this.updateCloseTime}
        updateRules={this.updateRules}
        updateMaxReservationLength={this.updateMaxReservationLength}
      />
    );
  }
}

export default graphql<Props, UpdateItemData, UpdateItemDataVariables>(
  gql`
    query UpdateItemData($id: String!) {
      itemData(id: $id) {
        id
        name
        description
        imgPath
        openTime
        closeTime
        maxReservationLength
        rules
        timeZone
        reservations {
          id
          startTime
          endTime
          user {
            email
          }
        }
      }
    }
  `,
  { options: props => ({ variables: { id: props.navigation.getParam("id") } }) }
)(UpdateItem);

const text: any = {
  textAlign: "center",
  fontSize: 20,
  padding: 10
};

const styles = StyleSheet.create({
  textEnabled: {
    color: "white",
    ...text
  },
  textDisabled: {
    color: colors.darkGray,
    ...text
  }
});
