/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UpdateItemData
// ====================================================

export interface UpdateItemData_itemData_reservations_user {
  __typename: "UserData";
  email: string;
}

export interface UpdateItemData_itemData_reservations {
  __typename: "Reservation";
  id: string;
  startTime: any;
  endTime: any;
  user: UpdateItemData_itemData_reservations_user;
}

export interface UpdateItemData_itemData {
  __typename: "Item";
  id: string;
  name: string;
  description: string | null;
  imgPath: string | null;
  openTime: number | null;
  closeTime: number | null;
  maxReservationLength: number | null;
  rules: string[];
  timeZone: string;
  reservations: UpdateItemData_itemData_reservations[];
}

export interface UpdateItemData {
  itemData: UpdateItemData_itemData | null;
}

export interface UpdateItemDataVariables {
  id: string;
}
