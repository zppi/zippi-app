/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateItemMutation
// ====================================================

export interface UpdateItemMutation_updateItem {
  __typename: "Item";
  id: string;
  name: string;
  description: string | null;
  openTime: number | null;
  closeTime: number | null;
}

export interface UpdateItemMutation {
  updateItem: UpdateItemMutation_updateItem;
}

export interface UpdateItemMutationVariables {
  id: string;
  name?: string | null;
  description?: string | null;
  openTime?: number | null;
  closeTime?: number | null;
  rules?: string[] | null;
  maxReservationLength?: number | null;
}
