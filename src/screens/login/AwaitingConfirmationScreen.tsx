import React, { ReactNode, ReactElement } from "react";
import { Alert, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { NavigationScreenProp } from "react-navigation";
import { Txt } from "../../components";
import { colors, containers } from "../../styles";
import routes from "../../navigation/routes";

interface Props {
  navigation: NavigationScreenProp<{ email: string }, any>;
}

export default class AwaitingConfirmationScreen extends React.Component<Props> {
  resendEmail = (): void => {
    Alert.alert(
      "Email Sent",
      `We've sent a new login email to ${this.props.navigation.getParam("email")}.`
    );

    setTimeout((): boolean => this.props.navigation.navigate(routes.appRoot), 1000);
  };

  render(): ReactNode {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar barStyle="dark-content" />
        <ScrollView contentContainerStyle={[containers.whiteBackground, { flex: 1 }]}>
          <Txt title style={styles.title}>
            Check your email
          </Txt>
          <Txt style={styles.body}>
            We've sent a login email to <Txt bold>{this.props.navigation.getParam("email")}</Txt>!
            Open your email on this device and click the link to finish logging in.
          </Txt>
          <View style={styles.buttons}>
            <Btn text="Go Back" onPress={() => this.props.navigation.goBack()} />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.darkBlue,
    flex: 1
  },
  title: { paddingBottom: 40 },
  body: { fontSize: 20 },
  buttons: {
    paddingBottom: 50,
    flex: 1,
    alignItems: "stretch",
    justifyContent: "flex-end"
  }
});

const Btn: React.FC<{ text: string; onPress: () => {} }> = ({ text, onPress }): ReactElement => (
  <TouchableOpacity style={btnStyles.container} onPress={onPress} activeOpacity={0.9}>
    <Txt style={btnStyles.label}>{text}</Txt>
  </TouchableOpacity>
);

const btnStyles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: colors.darkBlue,
    marginBottom: 10
  },
  label: {
    color: "white",
    fontSize: 24,
    textAlign: "center"
  }
});
