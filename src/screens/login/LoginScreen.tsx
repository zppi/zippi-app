import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { ReactNode } from "react";
import { Mutation } from "react-apollo";
import {
  Dimensions,
  LayoutAnimation,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Alert
} from "react-native";
import ScalableImage from "react-native-scalable-image";
import { NavigationScreenProp, ScrollView } from "react-navigation";
import { Linking } from "expo";
import gql from "graphql-tag";
import { Txt } from "../../components";
import { colors, fonts } from "../../styles";
import routes from "../../navigation/routes";
import { RequestLoginEmail, RequestLoginEmailVariables } from "./__generated__/RequestLoginEmail";
import { ApolloError } from "apollo-boost";
import { signIn } from "../../services/AuthService";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  email: string;
  pageLoaded: boolean;
  submitting: boolean;
}

const LOGIN_USER = gql`
  mutation RequestLoginEmail($email: String!, $callbackUrl: String!) {
    requestLoginEmail(email: $email, callbackUrl: $callbackUrl) {
      email
    }
  }
`;

export default class LoginScreen extends React.Component<Props, State> {
  state = { email: "", pageLoaded: false, submitting: false };

  inboundAnimation: NodeJS.Timeout | null = null;

  componentDidMount(): void {
    this.inboundAnimation = setTimeout(() => {
      LayoutAnimation.easeInEaseOut();
      this.setState({ pageLoaded: true });
    }, 200);
  }

  componentWillUnmount(): void {
    this.inboundAnimation && clearTimeout(this.inboundAnimation);
  }

  onSubmitCompleted = (data: RequestLoginEmail) => {
    this.setState({ submitting: false });
    this.props.navigation.navigate(routes.awaitingEmailConfirmation, {
      email: data.requestLoginEmail.email
    });
  };

  onSubmitError = (error: ApolloError) => {
    this.setState({ submitting: false });
    if (error.graphQLErrors && error.graphQLErrors[0]) {
      Alert.alert("Error", error.graphQLErrors[0].message);
    }
  };

  signInDev = () => __DEV__ && signIn("amanaplanacanal");

  render(): ReactNode {
    return (
      <ScrollView
        keyboardShouldPersistTaps="handled"
        keyboardDismissMode="on-drag"
        style={{ backgroundColor: colors.darkBlue }}
        contentContainerStyle={styles.wrapper}
      >
        <StatusBar barStyle="light-content" />
        <View>
          <ScalableImage
            width={Dimensions.get("window").width}
            source={require("../../../assets/splash.png")}
            style={styles.logo}
            resizeMode="contain"
          />
          {this.state.pageLoaded && (
            <View>
              <Txt title style={styles.subtitle} onPress={this.signInDev}>
                Reserve Anything!
              </Txt>
            </View>
          )}
        </View>
        {this.state.pageLoaded && (
          <>
            <Mutation<RequestLoginEmail, RequestLoginEmailVariables>
              mutation={LOGIN_USER}
              onCompleted={this.onSubmitCompleted}
              onError={this.onSubmitError}
            >
              {loginMutation => {
                const submitLogin = (): void => {
                  this.setState({ submitting: true });
                  loginMutation({
                    variables: {
                      email: this.state.email.trim(),
                      callbackUrl: Linking.makeUrl("emailVerified")
                    }
                  });
                };
                return (
                  <View style={styles.form}>
                    <Txt subtitle style={styles.instructions}>
                      Enter your email to get started.
                    </Txt>
                    <View style={styles.emailBox}>
                      <TextInput
                        textContentType="emailAddress"
                        returnKeyType="send"
                        style={styles.emailInput}
                        placeholder="Email"
                        placeholderTextColor="#ffffffcc"
                        selectionColor="white"
                        value={this.state.email}
                        onSubmitEditing={submitLogin}
                        onChangeText={email => this.setState({ email })}
                      />
                      <View style={{ paddingLeft: 10 }}>
                        {this.state.submitting ? (
                          <ActivityIndicator color="white" />
                        ) : (
                          <TouchableOpacity onPress={submitLogin}>
                            <MaterialCommunityIcons
                              name="arrow-right"
                              color="white"
                              size={30}
                              hitSlop={10}
                              style={{ paddingLeft: 10 }}
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </View>
                );
              }}
            </Mutation>
            <View />
            {/* Extra view to do the right thing with justifyContent: space-around */}
          </>
        )}
      </ScrollView>
    );
  }
}

const paddingHorizontal = 30;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "space-around"
  },
  logo: { width: "100%" },
  subtitle: {
    color: "white",
    textAlign: "center",
    textShadowColor: "rgba(0, 0, 0, 0.5)",
    textShadowOffset: { width: 2, height: 3 },
    textShadowRadius: 8,
    paddingBottom: 10
  },
  form: {
    paddingHorizontal
  },
  instructions: {
    color: "white",
    paddingBottom: 25
  },
  emailBox: {
    flexDirection: "row",
    alignItems: "center",

    borderWidth: 2,
    borderColor: "white",
    borderRadius: 3,
    padding: 10,
    paddingLeft: 15
  },
  emailInput: {
    color: "white",
    fontFamily: fonts.default,
    fontSize: 24,
    flex: 1
  }
});
