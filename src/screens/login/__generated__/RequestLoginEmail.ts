/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RequestLoginEmail
// ====================================================

export interface RequestLoginEmail_requestLoginEmail {
  __typename: "RequestLoginEmailPayload";
  email: string;
}

export interface RequestLoginEmail {
  requestLoginEmail: RequestLoginEmail_requestLoginEmail;
}

export interface RequestLoginEmailVariables {
  email: string;
  callbackUrl: string;
}
