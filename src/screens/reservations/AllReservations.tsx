import React, { ReactNode } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import ActionButton from "react-native-action-button";

import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { NavigationScreenProp } from "react-navigation";
import { Txt } from "../../components";
import { abIcon, colors, containers } from "../../styles";
import ReservationListItem from "./ReservationListItem";
import routes from "../../navigation/routes";
import { startOfHour, addHours, addDays } from "date-fns";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

export default class AllReservations extends React.Component<Props> {
  render(): ReactNode {
    return (
      <SafeAreaView style={{ height: "100%" }}>
        <ScrollView contentContainerStyle={containers.whiteBackground}>
          <Txt title style={{ paddingBottom: 30 }}>
            Reservations
          </Txt>
          <Txt subtitle style={{ paddingBottom: 18 }}>
            Upcoming
          </Txt>
          {reservations.map(r => (
            <ReservationListItem reservation={r} key={r.uuid} />
          ))}
        </ScrollView>
        <ActionButton buttonColor={colors.darkBlue} fixNativeFeedbackRadius>
          <ActionButton.Item
            buttonColor="#9b59b6"
            title="Scan Code"
            onPress={() => this.props.navigation.navigate(routes.scanCodeModal)}
          >
            <Ionicons name="md-qr-scanner" style={abIcon} />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="#3498db"
            title="Choose Item"
            onPress={() => this.props.navigation.navigate(routes.items)}
          >
            <MaterialCommunityIcons name="layers-outline" style={abIcon} />
          </ActionButton.Item>
        </ActionButton>
      </SafeAreaView>
    );
  }
}

// TODO replace this monstrosity with apollo codegen type
interface Reservation {
  uuid: any;
  startTime: any;
  endTime: any;
  item: any;
  user: any;
}

const reservations: Reservation[] = [
  {
    uuid: "15609157-e146-4c2e-9110-50d436eebf7c",
    startTime: addHours(startOfHour(new Date()), 1).toISOString(),
    endTime: addHours(startOfHour(new Date()), 2).toISOString(),
    item: {
      uuid: "25609157-e146-4c2e-9110-50d436eebf7c",
      name: "Centennial Barbecue",
      image_url:
        "https://c.pxhere.com/photos/ca/4b/grilling_hotdogs_hamburger_barbecue_grill_bbq_cooking_barbecue_grill-1054844.jpg!d"
    },
    user: { name: "Kyle Corbitt" }
  },
  {
    uuid: "35609157-e146-4c2e-9110-50d436eebf7c",
    startTime: addDays(startOfHour(new Date()), 2).toISOString(),
    endTime: addHours(addHours(startOfHour(new Date()), 2), 2).toISOString(),
    item: {
      uuid: "25609157-e146-4c2e-9110-50d436eebf7c",
      name: "Centennial Piano",
      image_url:
        "https://cdn11.bigcommerce.com/s-8wy6p2/images/stencil/1000x1000/products/6239/40108/364878.2__09875.1535041274.PNG?c=2"
    },
    user: { name: "Kyle Corbitt" }
  }
];
