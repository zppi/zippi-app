import React, { ReactNode } from "react";
import { Image, StyleSheet, View } from "react-native";
import TouchableScale from "react-native-touchable-scale";
import { Txt } from "../../components";
import { setElevation } from "../../styles";
import { formattedReservation } from "../../services/time";

interface Props {
  reservation: any; // TODO replace with Apollo codegen
}

const styles = StyleSheet.create({
  shadowWrapper: {
    ...setElevation(18),
    backgroundColor: "white",
    borderRadius: 10,
    marginBottom: 30
  },
  container: {
    backgroundColor: "white",
    borderRadius: 10,
    overflow: "hidden"
  },
  image: {
    width: "100%",
    height: undefined,
    aspectRatio: 2 / 1
  },
  detailsBox: {
    padding: 30
  },
  reservationTime: {
    fontSize: 12,
    textTransform: "uppercase",
    paddingBottom: 10
  },
  itemName: {
    fontSize: 24
  }
});

export default class ReservationListItem extends React.Component<Props> {
  render(): ReactNode {
    const r = this.props.reservation;

    return (
      <TouchableScale activeScale={0.95} style={styles.shadowWrapper} tension={250} friction={20}>
        <View style={styles.container}>
          <Image resizeMode="cover" style={styles.image} source={{ uri: r.item.image_url }} />
          <View style={styles.detailsBox}>
            <Txt bold style={styles.reservationTime}>
              {formattedReservation(r.startTime, r.endTime, "America/Los_Angeles")}
            </Txt>
            <Txt style={styles.itemName}>{r.item.name}</Txt>
          </View>
        </View>
      </TouchableScale>
    );
  }
}
