import React from "react";
import { range } from "lodash";
import { addHours, format, getHours, startOfDay } from "date-fns";
import { View, StyleSheet } from "react-native";
import { Txt } from "../../../components";

export const HOUR_HEIGHT = 50;

const AllHours = React.memo(() => (
  <>
    {range(0, 24).map(offset => {
      const h = addHours(startOfDay(new Date()), offset);
      return (
        <View key={format(h, "ha")} style={styles.box}>
          <View style={styles.labelBox}>
            <Txt style={styles.label}>{getHours(h) > 0 ? format(h, "ha") : ""}</Txt>
            <View style={styles.line}></View>
          </View>
        </View>
      );
    })}
  </>
));

export default AllHours;

export const lineColor = "#ccc";

const styles = StyleSheet.create({
  box: {
    height: HOUR_HEIGHT,
    overflow: "visible"
  },
  labelBox: {
    height: 30,
    marginTop: -15,
    flexDirection: "row",
    alignItems: "center"
  },
  label: {
    color: "#999",
    fontSize: 12,
    width: 40,
    marginRight: 5,
    textAlign: "right"
  },
  line: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: lineColor
  }
});
