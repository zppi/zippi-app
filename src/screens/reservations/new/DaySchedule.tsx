import React, { ReactNode } from "react";
import {
  roundToNearestMinutes,
  parseISO,
  addHours,
  differenceInSeconds,
  addSeconds,
  addMinutes,
  differenceInMinutes
} from "date-fns";
import {
  StyleProp,
  ViewStyle,
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  TouchableWithoutFeedback,
  GestureResponderEvent,
  NativeScrollEvent,
  NativeSyntheticEvent,
  TouchableOpacity,
  LayoutAnimation
} from "react-native";
import { Txt } from "../../../components";
import { colors, hitSlop } from "../../../styles";
import { LinearGradient } from "expo-linear-gradient";
import AllHours, { HOUR_HEIGHT, lineColor } from "./AllHours";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { NewReservationData_itemData } from "./__generated__/NewReservationData";
import { graphql, ChildProps } from "react-apollo";
import gql from "graphql-tag";
import { DayData, DayDataVariables } from "./__generated__/DayData";
import { tzFormat } from "../../../services/time";

const LEFT_MARGIN = 50;

interface Props {
  date: Date;
  timeZone: string;
  startTime?: Date;
  endTime?: Date;
  item: NewReservationData_itemData | null | undefined;
  onSetStartTime: (t: Date | undefined) => void;
  onSetEndTime: (t: Date | undefined) => void;
}

// @ts-ignore
@graphql<Props, DayData, DayDataVariables>(
  gql`
    query DayData($id: String!, $before: DateTime!, $after: DateTime!) {
      itemData(id: $id) {
        id
        reservations(after: $after, before: $before) {
          id
          startTime
          endTime
          user {
            id
            email
          }
        }
      }
    }
  `,
  {
    options: ({ item, date }) => ({
      variables: {
        id: item ? item.id : "",
        after: date.toISOString(),
        before: addHours(date, 24).toISOString()
      },
      fetchPolicy: "cache-and-network"
    }),
    skip: ({ item }) => item == null
  }
)
export default class DaySchedule extends React.PureComponent<
  ChildProps<Props, DayData, DayDataVariables>
> {
  // Time is a UTC date, OR seconds since midnight on the current date
  timeToY = (time: Date | number): number => {
    const seconds = time instanceof Date ? differenceInSeconds(time, this.props.date) : time;
    return (HOUR_HEIGHT * seconds) / 3600;
  };

  blockDimensions = (start: Date | number, end: Date | number): StyleProp<ViewStyle> => ({
    position: "absolute",
    left: LEFT_MARGIN + StyleSheet.hairlineWidth,
    right: 0,
    top: this.timeToY(start) + StyleSheet.hairlineWidth,
    bottom: this.timeToY(24 * 3600) - this.timeToY(end)
  });

  disabledBlock = (start: Date | number, end: Date | number): ReactNode => (
    <View style={[this.blockDimensions(start, end), styles.notAllowed]} />
  );

  onTouch = (e: GestureResponderEvent) => {
    if (this.viewHeight === null) return;

    if (this.props.endTime) {
      return; // Not clear that there's anything to do, so skip this
    }

    const viewOffset = Dimensions.get("window").height - this.viewHeight;
    const hours = (e.nativeEvent.pageY - viewOffset + this.scrollOffset) / HOUR_HEIGHT;
    const time = addMinutes(this.props.date, hours * 60);

    if (this.props.startTime) {
      // Round to nearest 30. Add 15 first so if you click in the middle
      // of a 15-minute block it assumes you want to include it.
      const roundedTime = roundToNearestMinutes(addSeconds(time, 15 * 60), { nearestTo: 30 });
      this.props.onSetEndTime(roundedTime);
    } else {
      // Round to nearest 30. Subtract 15 first so if you click in the middle
      // of a 30-minute block it assumes you want to include it.
      const roundedTime = roundToNearestMinutes(addSeconds(time, -15 * 60), { nearestTo: 30 });
      this.props.onSetStartTime(roundedTime);
    }
  };

  viewHeight: number | null = null;
  scrollOffset: number = 0;
  onScroll = (e: NativeSyntheticEvent<NativeScrollEvent>) =>
    (this.scrollOffset = e.nativeEvent.contentOffset.y);

  render(): ReactNode {
    const reservations = (
      (this.props.data && this.props.data.itemData && this.props.data.itemData.reservations) ||
      []
    ).map(r => ({ ...r, startTime: parseISO(r.startTime), endTime: parseISO(r.endTime) }));

    const rightNow = new Date();

    return (
      <>
        <View style={styles.leftLine} />
        <ScrollView
          contentContainerStyle={styles.container}
          onScrollEndDrag={this.onScroll}
          onMomentumScrollEnd={this.onScroll}
          onLayout={e => (this.viewHeight = e.nativeEvent.layout.height)}
        >
          <TouchableWithoutFeedback onPress={this.onTouch}>
            <View>
              <AllHours />
              {this.props.item &&
                this.props.item.openTime &&
                this.disabledBlock(0, this.props.item.openTime)}
              {this.props.item && this.props.item.closeTime && (
                <View
                  style={[
                    this.blockDimensions(this.props.item.closeTime, 24 * 3600),
                    styles.notAllowed
                  ]}
                />
              )}
              {reservations.map(r => (
                <View
                  style={[
                    styles.reservation,
                    styles.otherReservation,
                    this.blockDimensions(r.startTime, r.endTime)
                  ]}
                  key={r.id}
                >
                  <Txt bold style={styles.reservationText}>
                    {r.user.email}
                  </Txt>
                  {differenceInMinutes(r.endTime, r.startTime) > 30 && (
                    <Txt style={styles.reservationText}>
                      {tzFormat(r.startTime, "h:mmaaaaa", this.props.timeZone)} -{" "}
                      {tzFormat(r.endTime, "h:mmaaaaa", this.props.timeZone)}
                    </Txt>
                  )}
                </View>
              ))}
              {this.props.startTime &&
                (this.props.endTime ? (
                  // Show the complete reservation
                  <View
                    style={[
                      styles.reservation,
                      this.blockDimensions(this.props.startTime, this.props.endTime)
                    ]}
                  >
                    <CancelButton
                      onPress={() => {
                        LayoutAnimation.easeInEaseOut();
                        this.props.onSetStartTime(undefined);
                      }}
                    />
                    <Txt bold style={styles.reservationText}>
                      New Reservation
                    </Txt>
                    {differenceInMinutes(this.props.endTime, this.props.startTime) > 30 && (
                      <Txt style={styles.reservationText}>
                        {tzFormat(this.props.startTime, "h:mmaaaaa", this.props.timeZone)} -{" "}
                        {tzFormat(this.props.endTime, "h:mmaaaaa", this.props.timeZone)}
                      </Txt>
                    )}
                  </View>
                ) : (
                  // No end time, so show just a partial reservation
                  <View
                    style={[
                      styles.partialReservation,
                      this.blockDimensions(this.props.startTime, addHours(this.props.startTime, 1))
                    ]}
                  >
                    <View style={[styles.reservation, styles.partialReservationTop]}>
                      <CancelButton
                        onPress={() => {
                          LayoutAnimation.easeInEaseOut();
                          this.props.onSetStartTime(undefined);
                        }}
                      />
                      <Txt bold style={styles.reservationText}>
                        Select end time...
                      </Txt>
                    </View>
                    <LinearGradient
                      colors={[colors.primary, `${colors.primary}00`]}
                      style={{ flex: 1 }}
                    />
                  </View>
                ))}
              {rightNow > this.props.date && rightNow < addHours(this.props.date, 24) && (
                <View style={[styles.rightNowContainer, { top: this.timeToY(rightNow) }]}>
                  <View style={styles.rightNowDot} />
                </View>
              )}
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get("window").width
  },
  leftLine: {
    position: "absolute",
    left: LEFT_MARGIN,
    top: 0,
    height: "100%",
    width: StyleSheet.hairlineWidth,
    backgroundColor: lineColor
  },
  notAllowed: {
    backgroundColor: "#ccc9"
  },
  rightNowContainer: {
    position: "absolute",
    left: LEFT_MARGIN,
    right: 0,
    height: 2,
    backgroundColor: colors.text,
    justifyContent: "center"
  },
  rightNowDot: {
    height: 6,
    width: 6,
    borderRadius: 3,
    marginLeft: -3,
    backgroundColor: colors.text
  },
  reservation: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    marginHorizontal: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  otherReservation: {
    backgroundColor: colors.darkBlue
  },
  partialReservation: {
    marginHorizontal: 5
  },
  partialReservationTop: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    marginHorizontal: 0,
    flex: 2
  },
  reservationText: {
    color: "white"
  },
  cancelButton: {
    position: "absolute",
    right: 5,
    top: 5
  }
});

const CancelButton = React.memo((props: { onPress: () => void }) => (
  <TouchableOpacity onPress={props.onPress} hitSlop={hitSlop.large} style={styles.cancelButton}>
    <MaterialCommunityIcons name="close" size={18} color="white" />
  </TouchableOpacity>
));
