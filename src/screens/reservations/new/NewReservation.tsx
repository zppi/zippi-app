import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { ReactNode, ReactElement } from "react";
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  ViewToken,
  Platform
} from "react-native";
import { NavigationScreenProp, FlatList } from "react-navigation";
import { setElevation, colors, hitSlop } from "../../../styles";
import { range } from "lodash";
import { addDays, roundToNearestMinutes, addMinutes, max, differenceInMinutes } from "date-fns";
import DateTimePicker from "react-native-modal-datetime-picker";
import DaySchedule from "./DaySchedule";
import { Txt } from "../../../components";
import { graphql, ChildProps, Mutation } from "react-apollo";
import gql from "graphql-tag";
import {
  NewReservationData,
  NewReservationDataVariables
} from "./__generated__/NewReservationData";
import { startOfDayInTz, tzFormat } from "../../../services/time";
import { utcToZonedTime, zonedTimeToUtc } from "date-fns-tz";
import { CreateReservation, CreateReservationVariables } from "./__generated__/CreateReservation";
import { withDropdown } from "../../../navigation/DropdownContainer";
import { ITEM_DETAILS_QUERY } from "../../items/ItemDetails";
import Constants from "expo-constants";

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  startTime?: Date;
  endTime?: Date;
  zeroOffsetDate: Date;
  canSwipeBack: boolean;
  canSwipeForward: boolean;
  currentDateIndex: number;
  datePickerVisible: boolean;
  startTimePickerVisible: boolean;
  endTimePickerVisible: boolean;
}

const viewabilityConfig = {
  viewAreaCoveragePercentThreshold: 50
};

const MAX_SWIPE_SCREENS = 30;
const dayOffsets = range(-MAX_SWIPE_SCREENS, MAX_SWIPE_SCREENS);

// @ts-ignore
@graphql<Props, NewReservationData, NewReservationDataVariables>(
  gql`
    query NewReservationData($id: String!) {
      itemData(id: $id) {
        id
        timeZone
        openTime
        closeTime
      }
    }
  `,
  { options: ({ navigation }) => ({ variables: { id: navigation.getParam("itemId") } }) }
)
export default class NewReservation extends React.Component<
  ChildProps<Props, NewReservationData, NewReservationDataVariables>,
  State
> {
  constructor(props: Props) {
    super(props);

    this.timeZone = props.navigation.getParam("timeZone");
    this.state = {
      canSwipeBack: true,
      canSwipeForward: true,
      zeroOffsetDate: startOfDayInTz(new Date(), this.timeZone),
      currentDateIndex: MAX_SWIPE_SCREENS,
      datePickerVisible: false,
      startTimePickerVisible: false,
      endTimePickerVisible: false
    };
  }

  timeZone: string;

  renderDateControl = () => (
    <View style={styles.dateControl}>
      <TouchableOpacity
        onPress={() =>
          this.horizontalList &&
          this.horizontalList.scrollToIndex({ index: this.state.currentDateIndex - 1 })
        }
        disabled={!this.state.canSwipeBack}
      >
        <MaterialCommunityIcons
          name="chevron-left"
          style={[styles.dateChanger, !this.state.canSwipeBack && styles.disabledButton]}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => this.setState({ datePickerVisible: true })}
        hitSlop={hitSlop.large}
      >
        <Txt style={styles.textButton}>
          {tzFormat(this.currentDate(), "MMM d", this.timeZone).toUpperCase()}
        </Txt>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() =>
          this.horizontalList &&
          this.horizontalList.scrollToIndex({ index: this.state.currentDateIndex + 1 })
        }
        disabled={!this.state.canSwipeForward}
      >
        <MaterialCommunityIcons
          name="chevron-right"
          style={[styles.dateChanger, !this.state.canSwipeForward && { color: "#ccc" }]}
        />
      </TouchableOpacity>
      <DateTimePicker
        isVisible={this.state.datePickerVisible}
        onConfirm={this.onDatePicked}
        onCancel={() => this.setState({ datePickerVisible: false })}
      />
    </View>
  );

  onDatePicked = (date: Date) => {
    this.setState({ zeroOffsetDate: date, datePickerVisible: false });
    this.horizontalList &&
      this.horizontalList.scrollToIndex({ index: MAX_SWIPE_SCREENS, animated: false });
  };

  onStartTimePicked = (startTime: Date) => {
    this.setState({
      startTimePickerVisible: false,
      startTime,
      endTime: undefined
    });
  };

  onEndTimePicked = (date: Date) => {
    const endTime = max([addMinutes(this.state.startTime || new Date(), 15), date]);
    this.setState({
      endTime,
      endTimePickerVisible: false
    });
  };

  renderTimesControls = () => {
    const minutesSinceMorning = differenceInMinutes(
      new Date(),
      startOfDayInTz(new Date(), this.timeZone)
    );
    const defaultStart = roundToNearestMinutes(
      addMinutes(this.currentDate(), minutesSinceMorning),
      { nearestTo: 15 }
    );
    const defaultEnd = addMinutes(this.state.startTime || defaultStart, 60);

    return (
      <View style={styles.timeControls}>
        <TouchableOpacity
          onPress={() => this.setState({ startTimePickerVisible: true })}
          hitSlop={hitSlop.large}
        >
          <Txt style={[styles.textButton, !this.state.startTime && styles.disabledButton]}>
            {this.state.startTime
              ? tzFormat(this.state.startTime, "h:mma", this.timeZone).toUpperCase()
              : "START"}
          </Txt>
        </TouchableOpacity>
        <Txt style={[styles.textButton, { color: colors.text, marginHorizontal: 2 }]}>-</Txt>
        <TouchableOpacity
          onPress={() => this.setState({ endTimePickerVisible: true })}
          hitSlop={hitSlop.large}
          disabled={this.state.startTime == undefined}
        >
          <Txt style={[styles.textButton, !this.state.endTime && styles.disabledButton]}>
            {this.state.endTime
              ? tzFormat(this.state.endTime, "h:mma", this.timeZone).toUpperCase()
              : "END"}
          </Txt>
        </TouchableOpacity>
        <DateTimePicker
          isVisible={this.state.startTimePickerVisible}
          onConfirm={(d: Date) => this.onStartTimePicked(zonedTimeToUtc(d, this.timeZone))}
          onCancel={() => this.setState({ startTimePickerVisible: false })}
          date={utcToZonedTime(this.state.startTime || defaultStart, this.timeZone)}
          mode="time"
          minuteInterval={15}
          titleIOS="Start time"
        />
        <DateTimePicker
          isVisible={this.state.endTimePickerVisible}
          onConfirm={(d: Date) => this.onEndTimePicked(zonedTimeToUtc(d, this.timeZone))}
          onCancel={() => this.setState({ endTimePickerVisible: false })}
          date={utcToZonedTime(this.state.endTime || defaultEnd, this.timeZone)}
          mode="time"
          minuteInterval={15}
          titleIOS="End time"
        />
      </View>
    );
  };

  offsetToDate = (offset: number): Date => addDays(this.state.zeroOffsetDate, offset);
  currentDate = (): Date => this.offsetToDate(dayOffsets[this.state.currentDateIndex]);

  horizontalList: FlatList<number> | null;
  onSwipe = ({ viewableItems }: { viewableItems: ViewToken[] }) => {
    if (viewableItems.length === 0 || viewableItems[0].index === null) {
      return;
    }

    const { index } = viewableItems[0];
    this.setState({
      currentDateIndex: index,
      canSwipeBack: index > 0,
      canSwipeForward: index < MAX_SWIPE_SCREENS * 2 - 1,
      startTime: undefined,
      endTime: undefined
    });
  };

  renderDay = ({ item, index }: { item: number; index: number }): ReactElement => {
    if (index < this.state.currentDateIndex - 1 || index > this.state.currentDateIndex + 1) {
      // Don't render screens that are too far from the currently visible one to aid performance
      return <View style={{ width: Dimensions.get("screen").width }} />;
    }

    // this date should be the beginning of the day in the item's timezone
    const date = startOfDayInTz(this.offsetToDate(item), this.timeZone);

    return (
      <DaySchedule
        date={date}
        timeZone={this.timeZone}
        item={this.props.data && this.props.data.itemData}
        startTime={this.state.startTime}
        endTime={this.state.endTime}
        onSetStartTime={this.onStartTimePicked}
        onSetEndTime={this.onEndTimePicked}
      />
    );
  };

  onComplete = () => {
    withDropdown(d => d.alertWithType("success", "Success", "Reservation created!"));
    this.props.navigation.goBack();
  };

  render(): ReactNode {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor="#ffffff" translucent={false} />
        <View style={styles.header}>
          <View style={styles.titleLine}>
            <MaterialCommunityIcons
              name="calendar-plus"
              size={20}
              color={colors.text}
              style={styles.calendarIcon}
            />
            <Txt title style={styles.title}>
              New Reservation
            </Txt>
            <Mutation<CreateReservation, CreateReservationVariables>
              mutation={gql`
                mutation CreateReservation(
                  $itemId: String!
                  $startTime: DateTime!
                  $endTime: DateTime!
                ) {
                  createReservation(itemId: $itemId, startTime: $startTime, endTime: $endTime) {
                    id
                  }
                }
              `}
              onCompleted={this.onComplete}
              variables={{
                itemId:
                  (this.props.data && this.props.data.itemData && this.props.data.itemData.id) ||
                  "",
                startTime: this.state.startTime || new Date(),
                endTime: this.state.endTime || new Date()
              }}
              refetchQueries={[
                {
                  query: ITEM_DETAILS_QUERY,
                  variables: {
                    id: this.props.data && this.props.data.itemData && this.props.data.itemData.id
                  }
                }
              ]}
            >
              {(createReservation, { loading }) => {
                const saveEnabled = this.state.startTime && this.state.endTime && !loading;

                return (
                  <TouchableOpacity
                    onPress={() => createReservation()}
                    hitSlop={hitSlop.large}
                    style={{ marginRight: 15 }}
                    disabled={!saveEnabled}
                  >
                    <MaterialCommunityIcons
                      name="check"
                      size={30}
                      color={saveEnabled ? colors.success : colors.lightGray}
                    />
                  </TouchableOpacity>
                );
              }}
            </Mutation>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              hitSlop={hitSlop.large}
            >
              <MaterialCommunityIcons name="close" size={30} />
            </TouchableOpacity>
          </View>
          <View style={styles.controlsLine}>
            {this.renderDateControl()}
            {this.renderTimesControls()}
          </View>
        </View>
        <FlatList
          ref={horizontalList => (this.horizontalList = horizontalList)}
          horizontal
          pagingEnabled
          data={dayOffsets}
          extraData={this.state.zeroOffsetDate}
          renderItem={this.renderDay}
          onViewableItemsChanged={this.onSwipe}
          viewabilityConfig={viewabilityConfig}
          keyExtractor={offset => this.offsetToDate(offset).toISOString()}
          initialScrollIndex={MAX_SWIPE_SCREENS}
          getItemLayout={(_, index) => ({
            length: Dimensions.get("window").width,
            offset: Dimensions.get("window").width * index,
            index
          })}
          showsHorizontalScrollIndicator={false}
          initialNumToRender={1}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    overflow: "hidden",
    flex: 1
  },
  header: {
    ...setElevation(10),
    backgroundColor: "white",
    padding: 10,
    marginTop: -200, // Cover up the header so our shadow doesn't produce an ugly contrast

    // We should just make the status bar opaque on Android to avoid this issue, but apparently impossible
    // https://github.com/expo/expo/issues/2813
    paddingTop: 200 + 10 + (Platform.OS === "android" ? Constants.statusBarHeight : 0)
  },
  titleLine: {
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: 10
  },
  calendarIcon: {
    paddingTop: 3,
    paddingRight: 3
  },
  title: {
    flex: 1,
    fontSize: 20
  },
  scrollView: {
    flex: 1
  },
  controlsLine: {
    flexDirection: "row"
  },
  dateControl: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1
  },
  dateChanger: {
    fontSize: 35,
    color: colors.text
  },
  textButton: {
    fontSize: 20,
    lineHeight: 20,
    color: colors.darkBlue,
    textAlign: "center"
  },
  disabledButton: {
    color: "#ccc"
  },
  timeControls: {
    flexDirection: "row",
    alignItems: "center"
  }
});
