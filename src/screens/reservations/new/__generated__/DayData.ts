/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: DayData
// ====================================================

export interface DayData_itemData_reservations_user {
  __typename: "UserData";
  id: string;
  email: string;
}

export interface DayData_itemData_reservations {
  __typename: "Reservation";
  id: string;
  startTime: any;
  endTime: any;
  user: DayData_itemData_reservations_user;
}

export interface DayData_itemData {
  __typename: "Item";
  id: string;
  reservations: DayData_itemData_reservations[];
}

export interface DayData {
  itemData: DayData_itemData | null;
}

export interface DayDataVariables {
  id: string;
  before: any;
  after: any;
}
