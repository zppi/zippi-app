/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ItemData
// ====================================================

export interface ItemData_itemData {
  __typename: "Item";
  id: string;
  timeZone: string;
  openTime: number | null;
  closeTime: number | null;
}

export interface ItemData {
  itemData: ItemData_itemData | null;
}

export interface ItemDataVariables {
  id: string;
}
