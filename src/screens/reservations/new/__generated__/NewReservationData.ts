/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: NewReservationData
// ====================================================

export interface NewReservationData_itemData {
  __typename: "Item";
  id: string;
  timeZone: string;
  openTime: number | null;
  closeTime: number | null;
}

export interface NewReservationData {
  itemData: NewReservationData_itemData | null;
}

export interface NewReservationDataVariables {
  id: string;
}
