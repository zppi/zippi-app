import { AsyncStorage } from "react-native";
import NavigationService from "../navigation/NavigationService";

const SESSION_TOKEN_KEY = "@AuthService/session_token";

let authLoaded = false;

let sessionToken: string | null = null;

export const signIn = (token: string): void => {
  AsyncStorage.setItem(SESSION_TOKEN_KEY, token);
  sessionToken = token;
  authLoaded = true;
  NavigationService.navigate(NavigationService.routes.appRoot);
};

export const signOut = (): void => {
  AsyncStorage.removeItem(SESSION_TOKEN_KEY);
  sessionToken = null;
  NavigationService.navigate(NavigationService.routes.authRoot);
};

export const getSessionToken = async (): Promise<typeof sessionToken> => {
  if (authLoaded) {
    return sessionToken;
  }

  sessionToken = await AsyncStorage.getItem(SESSION_TOKEN_KEY);
  authLoaded = true;
  return sessionToken;
};

export const isSignedIn = async (): Promise<boolean> => (await getSessionToken()) !== null;
