import { tzDifference, startOfDayInTz } from "./time";

test("tzDifference in different timezones", () => {
  expect(tzDifference("America/Los_Angeles", "America/New_York")).toEqual(3 * 60 * 60);
});

test("tzDifference in the same timezone", () => {
  expect(tzDifference("America/Los_Angeles", "America/Los_Angeles")).toEqual(0);
});

test("startOfDayInTz works", () => {
  expect(
    startOfDayInTz(new Date("2019-07-05T14:00:00Z"), "Pacific/Auckland").toISOString()
  ).toEqual("2019-07-05T12:00:00.000Z");
});
