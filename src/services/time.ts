import { parseISO, isSameDay, differenceInSeconds, format, startOfDay } from "date-fns";
import { format as formatInTz } from "date-fns-tz";
import { zonedTimeToUtc, utcToZonedTime } from "date-fns-tz";

// Returns the number of seconds that need to be added to a time in the first timezone
// to get the equivalent wall-clock time in the second timezone for today's date.
// Not sure if we'll ever actually want to use this function, but leaving it in as an example
// of how tests work.
export function tzDifference(
  tz1: string,
  tz2: string = Intl.DateTimeFormat().resolvedOptions().timeZone
): number {
  const base = new Date();

  const convertedTime = utcToZonedTime(zonedTimeToUtc(base, tz2), tz1);

  return differenceInSeconds(base, convertedTime);
}

export const startOfDayInTz = (date: Date, timeZone: string): Date =>
  zonedTimeToUtc(startOfDay(utcToZonedTime(date, timeZone)), timeZone);

// Formats a UTC date as if you were formatting it locally in the given timezone.
export const tzFormat = (date: Date, fmtString: string, timeZone: string): string =>
  formatInTz(utcToZonedTime(date, timeZone), fmtString, { timeZone });

export const formattedReservation = (
  startTime: string,
  endTime: string,
  timeZone: string
): string => {
  const start = parseISO(startTime);
  const end = parseISO(endTime);
  if (tzDifference(timeZone) === 0) {
    // User is in the same timezone as the item
    const dateString = isSameDay(new Date(), start) ? "Today" : format(start, "EEEE MMMM d");
    return `${dateString} ${format(start, "h:mmaaaaa")} – ${format(end, "h:mmaaaaa")}`;
  } else {
    // Item's timezone doesn't match the user's, so display the timezone explicitly
    return `${tzFormat(start, "EEEE MMMM d h:mmaaaaa", timeZone)} – ${tzFormat(
      end,
      "h:mmaaaaa zzz",
      timeZone
    )}`;
  }
};
