import { StyleSheet, ViewStyle } from "react-native";

// Palette: https://colorhunt.co/palette/145446
export const colors = {
  primary: "#ff8246",
  accent: "#373a6d",
  darkBlue: "#6fc2d0",
  lightBlue: "#beeef7",
  success: "#28a745",
  text: "#333",
  lightGray: "#cdcdcd",
  darkGray: "#60625f",
  borderGray: "#d6d7da",
  green: "#55CF47"
};

export const fonts = {
  default: "custom-font",
  italic: "custom-font-italic",
  bold: "custom-font-bold"
};

// Adapted from https://ethercreative.github.io/react-native-shadow-generator/
export const setElevation = (el: number): ViewStyle => ({
  elevation: el,
  shadowColor: "#aaa",
  shadowOffset: {
    width: 0,
    height: 1 + el / 2
  },
  shadowRadius: (el * 2) / 3,
  shadowOpacity: 0.18 + (0.4 * el) / 24
});

export const { abIcon } = StyleSheet.create({
  abIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});

export const containers = StyleSheet.create({
  whiteBackground: {
    paddingHorizontal: 30,
    paddingTop: 50
  }
});

export const hitSlop = {
  large: { top: 10, left: 10, right: 10, bottom: 10 }
};
