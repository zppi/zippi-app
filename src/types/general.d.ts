// https://github.com/Microsoft/TypeScript/issues/30471#issuecomment-474963436
declare module "console" {
  export = typeof import("console");
}

// https://github.com/microsoft/TypeScript/issues/13348#issuecomment-282388407
declare module "*";
